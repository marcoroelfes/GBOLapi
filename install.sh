#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Pull latest
git -C "$DIR" pull

gradle install -b "$DIR/build.gradle" 

cp $DIR/build/libs/GBOLApi.jar $DIR/

#Comment: Null pointer issue
if [ -d $DIR/../RGBOLApi ]; then
  cp $DIR/GBOLApi.jar $DIR/../RGBOLApi/inst/java/
fi

