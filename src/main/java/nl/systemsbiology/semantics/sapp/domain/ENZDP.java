package nl.systemsbiology.semantics.sapp.domain;

import java.lang.Float;
import java.lang.String;
import java.util.List;
import life.gbol.domain.ProvenanceAnnotation;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public interface ENZDP extends ProvenanceAnnotation {
  String getRatio();

  void setRatio(String val);

  void remPattern(String val);

  List<? extends String> getAllPattern();

  void addPattern(String val);

  Float getMaxbitscore();

  void setMaxbitscore(Float val);

  String getSupport();

  void setSupport(String val);

  String getAmbigous();

  void setAmbigous(String val);

  Float getLikelihoodscore();

  void setLikelihoodscore(Float val);
}
