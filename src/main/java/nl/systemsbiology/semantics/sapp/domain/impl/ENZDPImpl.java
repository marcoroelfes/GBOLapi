package nl.systemsbiology.semantics.sapp.domain.impl;

import java.lang.Float;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Feature;
import life.gbol.domain.impl.ProvenanceAnnotationImpl;
import nl.systemsbiology.semantics.sapp.domain.ENZDP;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public class ENZDPImpl extends ProvenanceAnnotationImpl implements ENZDP {
  public static final String TypeIRI = "http://semantics.systemsbiology.nl/sapp/0.1/ENZDP";

  protected ENZDPImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ENZDP make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ENZDPImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ENZDP.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ENZDP.class,false);
          if(toRet == null) {
            toRet = new ENZDPImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ENZDP)) {
          throw new RuntimeException("Instance of nl.systemsbiology.semantics.sapp.domain.impl.ENZDPImpl expected");
        }
      }
      return (ENZDP)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/ratio");
    this.checkCardMin1("http://gbol.life/0.1/maxbitscore");
    this.checkCardMin1("http://gbol.life/0.1/support");
    this.checkCardMin1("http://gbol.life/0.1/ambigous");
    this.checkCardMin1("http://gbol.life/0.1/likelihoodscore");
  }

  public String getRatio() {
    return this.getStringLit("http://gbol.life/0.1/ratio",false);
  }

  public void setRatio(String val) {
    this.setStringLit("http://gbol.life/0.1/ratio",val);
  }

  public void remPattern(String val) {
    this.remStringLit("http://gbol.life/0.1/pattern",val,true);
  }

  public List<? extends String> getAllPattern() {
    return this.getStringLitSet("http://gbol.life/0.1/pattern",true);
  }

  public void addPattern(String val) {
    this.addStringLit("http://gbol.life/0.1/pattern",val);
  }

  public Float getMaxbitscore() {
    return this.getFloatLit("http://gbol.life/0.1/maxbitscore",false);
  }

  public void setMaxbitscore(Float val) {
    this.setFloatLit("http://gbol.life/0.1/maxbitscore",val);
  }

  public String getSupport() {
    return this.getStringLit("http://gbol.life/0.1/support",false);
  }

  public void setSupport(String val) {
    this.setStringLit("http://gbol.life/0.1/support",val);
  }

  public String getAmbigous() {
    return this.getStringLit("http://gbol.life/0.1/ambigous",false);
  }

  public void setAmbigous(String val) {
    this.setStringLit("http://gbol.life/0.1/ambigous",val);
  }

  public Float getLikelihoodscore() {
    return this.getFloatLit("http://gbol.life/0.1/likelihoodscore",false);
  }

  public void setLikelihoodscore(Float val) {
    this.setFloatLit("http://gbol.life/0.1/likelihoodscore",val);
  }

  public void remReference(Document val) {
    this.remRef("http://gbol.life/0.1/reference",val,true);
  }

  public List<? extends Document> getAllReference() {
    return this.getRefSet("http://gbol.life/0.1/reference",true,Document.class);
  }

  public void addReference(Document val) {
    this.addRef("http://gbol.life/0.1/reference",val);
  }

  public void remDerivedFrom(Feature val) {
    this.remRef("http://gbol.life/0.1/derivedFrom",val,true);
  }

  public List<? extends Feature> getAllDerivedFrom() {
    return this.getRefSet("http://gbol.life/0.1/derivedFrom",true,Feature.class);
  }

  public void addDerivedFrom(Feature val) {
    this.addRef("http://gbol.life/0.1/derivedFrom",val);
  }

  public String getProvenanceNote() {
    return this.getStringLit("http://gbol.life/0.1/provenanceNote",true);
  }

  public void setProvenanceNote(String val) {
    this.setStringLit("http://gbol.life/0.1/provenanceNote",val);
  }

  public String getExperiment() {
    return this.getStringLit("http://gbol.life/0.1/experiment",true);
  }

  public void setExperiment(String val) {
    this.setStringLit("http://gbol.life/0.1/experiment",val);
  }
}
