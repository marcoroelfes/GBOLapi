package nl.systemsbiology.semantics.sapp.domain;

import java.lang.Double;
import java.lang.String;
import life.gbol.domain.ProvenanceAnnotation;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public interface InterProScan extends ProvenanceAnnotation {
  Double getEvalue();

  void setEvalue(Double val);

  String getFamilyName();

  void setFamilyName(String val);

  Double getScore();

  void setScore(Double val);
}
