package nl.systemsbiology.semantics.sapp.domain;

import life.gbol.domain.ProvenanceAnnotation;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public interface Aragorn extends ProvenanceAnnotation {
}
