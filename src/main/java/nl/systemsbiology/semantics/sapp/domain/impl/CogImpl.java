package nl.systemsbiology.semantics.sapp.domain.impl;

import java.lang.Double;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Feature;
import nl.systemsbiology.semantics.sapp.domain.Cog;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public class CogImpl extends BlastImpl implements Cog {
  public static final String TypeIRI = "http://semantics.systemsbiology.nl/sapp/0.1/Cog";

  protected CogImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Cog make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new CogImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Cog.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Cog.class,false);
          if(toRet == null) {
            toRet = new CogImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Cog)) {
          throw new RuntimeException("Instance of nl.systemsbiology.semantics.sapp.domain.impl.CogImpl expected");
        }
      }
      return (Cog)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public String getLetter() {
    return this.getStringLit("http://gbol.life/0.1/letter",true);
  }

  public void setLetter(String val) {
    this.setStringLit("http://gbol.life/0.1/letter",val);
  }

  public String getMembershipClass() {
    return this.getStringLit("http://gbol.life/0.1/membershipClass",true);
  }

  public void setMembershipClass(String val) {
    this.setStringLit("http://gbol.life/0.1/membershipClass",val);
  }

  public String getLabel() {
    return this.getStringLit("http://gbol.life/0.1/label",true);
  }

  public void setLabel(String val) {
    this.setStringLit("http://gbol.life/0.1/label",val);
  }

  public String getOrganismName() {
    return this.getStringLit("http://gbol.life/0.1/organismName",true);
  }

  public void setOrganismName(String val) {
    this.setStringLit("http://gbol.life/0.1/organismName",val);
  }

  public Double getEvalue() {
    return this.getDoubleLit("http://gbol.life/0.1/evalue",true);
  }

  public void setEvalue(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/evalue",val);
  }

  public Integer getMismatches() {
    return this.getIntegerLit("http://gbol.life/0.1/mismatches",true);
  }

  public void setMismatches(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/mismatches",val);
  }

  public Float getBitscore() {
    return this.getFloatLit("http://gbol.life/0.1/bitscore",true);
  }

  public void setBitscore(Float val) {
    this.setFloatLit("http://gbol.life/0.1/bitscore",val);
  }

  public Integer getGaps() {
    return this.getIntegerLit("http://gbol.life/0.1/gaps",true);
  }

  public void setGaps(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/gaps",val);
  }

  public Float getPercIdentity() {
    return this.getFloatLit("http://gbol.life/0.1/percIdentity",true);
  }

  public void setPercIdentity(Float val) {
    this.setFloatLit("http://gbol.life/0.1/percIdentity",val);
  }

  public Integer getAlignmentLength() {
    return this.getIntegerLit("http://gbol.life/0.1/alignmentLength",true);
  }

  public void setAlignmentLength(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/alignmentLength",val);
  }

  public void remReference(Document val) {
    this.remRef("http://gbol.life/0.1/reference",val,true);
  }

  public List<? extends Document> getAllReference() {
    return this.getRefSet("http://gbol.life/0.1/reference",true,Document.class);
  }

  public void addReference(Document val) {
    this.addRef("http://gbol.life/0.1/reference",val);
  }

  public void remDerivedFrom(Feature val) {
    this.remRef("http://gbol.life/0.1/derivedFrom",val,true);
  }

  public List<? extends Feature> getAllDerivedFrom() {
    return this.getRefSet("http://gbol.life/0.1/derivedFrom",true,Feature.class);
  }

  public void addDerivedFrom(Feature val) {
    this.addRef("http://gbol.life/0.1/derivedFrom",val);
  }

  public String getProvenanceNote() {
    return this.getStringLit("http://gbol.life/0.1/provenanceNote",true);
  }

  public void setProvenanceNote(String val) {
    this.setStringLit("http://gbol.life/0.1/provenanceNote",val);
  }

  public String getExperiment() {
    return this.getStringLit("http://gbol.life/0.1/experiment",true);
  }

  public void setExperiment(String val) {
    this.setStringLit("http://gbol.life/0.1/experiment",val);
  }
}
