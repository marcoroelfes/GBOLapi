package nl.systemsbiology.semantics.sapp.domain;

import java.lang.Float;
import java.lang.String;
import life.gbol.domain.ProvenanceAnnotation;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public interface Prodigal extends ProvenanceAnnotation {
  String getRbsMotif();

  void setRbsMotif(String val);

  String getGcContent();

  void setGcContent(String val);

  Float getConf();

  void setConf(Float val);

  Float getRScore();

  void setRScore(Float val);

  Float getScore();

  void setScore(Float val);

  Float getSScore();

  void setSScore(Float val);

  Float getCScore();

  void setCScore(Float val);

  Float getTScore();

  void setTScore(Float val);

  String getStartType();

  void setStartType(String val);

  String getRbsSpacer();

  void setRbsSpacer(String val);

  Float getUScore();

  void setUScore(Float val);
}
