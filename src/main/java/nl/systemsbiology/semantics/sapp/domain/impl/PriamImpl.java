package nl.systemsbiology.semantics.sapp.domain.impl;

import java.lang.Double;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Feature;
import life.gbol.domain.Region;
import life.gbol.domain.impl.ProvenanceAnnotationImpl;
import nl.systemsbiology.semantics.sapp.domain.Priam;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public class PriamImpl extends ProvenanceAnnotationImpl implements Priam {
  public static final String TypeIRI = "http://semantics.systemsbiology.nl/sapp/0.1/Priam";

  protected PriamImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Priam make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PriamImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Priam.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Priam.class,false);
          if(toRet == null) {
            toRet = new PriamImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Priam)) {
          throw new RuntimeException("Instance of nl.systemsbiology.semantics.sapp.domain.impl.PriamImpl expected");
        }
      }
      return (Priam)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/profileId");
    this.checkCardMin1("http://gbol.life/0.1/profileRegioin");
    this.checkCardMin1("http://gbol.life/0.1/profileProportion");
    this.checkCardMin1("http://gbol.life/0.1/evalue");
    this.checkCardMin1("http://gbol.life/0.1/bitScore");
    this.checkCardMin1("http://gbol.life/0.1/sourceRegion");
    this.checkCardMin1("http://gbol.life/0.1/positiveHitProbability");
    this.checkCardMin1("http://gbol.life/0.1/foundCatalyticPattern");
    this.checkCardMin1("http://gbol.life/0.1/isBestOverlap");
  }

  public String getProfileId() {
    return this.getStringLit("http://gbol.life/0.1/profileId",false);
  }

  public void setProfileId(String val) {
    this.setStringLit("http://gbol.life/0.1/profileId",val);
  }

  public Region getProfileRegioin() {
    return this.getRef("http://gbol.life/0.1/profileRegioin",false,Region.class);
  }

  public void setProfileRegioin(Region val) {
    this.setRef("http://gbol.life/0.1/profileRegioin",val,Region.class);
  }

  public Float getProfileProportion() {
    return this.getFloatLit("http://gbol.life/0.1/profileProportion",false);
  }

  public void setProfileProportion(Float val) {
    this.setFloatLit("http://gbol.life/0.1/profileProportion",val);
  }

  public Double getEvalue() {
    return this.getDoubleLit("http://gbol.life/0.1/evalue",false);
  }

  public void setEvalue(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/evalue",val);
  }

  public Float getBitScore() {
    return this.getFloatLit("http://gbol.life/0.1/bitScore",false);
  }

  public void setBitScore(Float val) {
    this.setFloatLit("http://gbol.life/0.1/bitScore",val);
  }

  public Region getSourceRegion() {
    return this.getRef("http://gbol.life/0.1/sourceRegion",false,Region.class);
  }

  public void setSourceRegion(Region val) {
    this.setRef("http://gbol.life/0.1/sourceRegion",val,Region.class);
  }

  public String getQueryStrand() {
    return this.getStringLit("http://gbol.life/0.1/queryStrand",true);
  }

  public void setQueryStrand(String val) {
    this.setStringLit("http://gbol.life/0.1/queryStrand",val);
  }

  public Float getPositiveHitProbability() {
    return this.getFloatLit("http://gbol.life/0.1/positiveHitProbability",false);
  }

  public void setPositiveHitProbability(Float val) {
    this.setFloatLit("http://gbol.life/0.1/positiveHitProbability",val);
  }

  public String getFoundCatalyticPattern() {
    return this.getStringLit("http://gbol.life/0.1/foundCatalyticPattern",false);
  }

  public void setFoundCatalyticPattern(String val) {
    this.setStringLit("http://gbol.life/0.1/foundCatalyticPattern",val);
  }

  public String getIsBestOverlap() {
    return this.getStringLit("http://gbol.life/0.1/isBestOverlap",false);
  }

  public void setIsBestOverlap(String val) {
    this.setStringLit("http://gbol.life/0.1/isBestOverlap",val);
  }

  public Integer getProfileLength() {
    return this.getIntegerLit("http://gbol.life/0.1/profileLength",true);
  }

  public void setProfileLength(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/profileLength",val);
  }

  public Integer getAlignLength() {
    return this.getIntegerLit("http://gbol.life/0.1/alignLength",true);
  }

  public void setAlignLength(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/alignLength",val);
  }

  public void remReference(Document val) {
    this.remRef("http://gbol.life/0.1/reference",val,true);
  }

  public List<? extends Document> getAllReference() {
    return this.getRefSet("http://gbol.life/0.1/reference",true,Document.class);
  }

  public void addReference(Document val) {
    this.addRef("http://gbol.life/0.1/reference",val);
  }

  public void remDerivedFrom(Feature val) {
    this.remRef("http://gbol.life/0.1/derivedFrom",val,true);
  }

  public List<? extends Feature> getAllDerivedFrom() {
    return this.getRefSet("http://gbol.life/0.1/derivedFrom",true,Feature.class);
  }

  public void addDerivedFrom(Feature val) {
    this.addRef("http://gbol.life/0.1/derivedFrom",val);
  }

  public String getProvenanceNote() {
    return this.getStringLit("http://gbol.life/0.1/provenanceNote",true);
  }

  public void setProvenanceNote(String val) {
    this.setStringLit("http://gbol.life/0.1/provenanceNote",val);
  }

  public String getExperiment() {
    return this.getStringLit("http://gbol.life/0.1/experiment",true);
  }

  public void setExperiment(String val) {
    this.setStringLit("http://gbol.life/0.1/experiment",val);
  }
}
