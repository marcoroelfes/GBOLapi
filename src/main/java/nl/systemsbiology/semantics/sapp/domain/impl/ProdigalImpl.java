package nl.systemsbiology.semantics.sapp.domain.impl;

import java.lang.Float;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Feature;
import life.gbol.domain.impl.ProvenanceAnnotationImpl;
import nl.systemsbiology.semantics.sapp.domain.Prodigal;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public class ProdigalImpl extends ProvenanceAnnotationImpl implements Prodigal {
  public static final String TypeIRI = "http://semantics.systemsbiology.nl/sapp/0.1/Prodigal";

  protected ProdigalImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Prodigal make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ProdigalImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Prodigal.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Prodigal.class,false);
          if(toRet == null) {
            toRet = new ProdigalImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Prodigal)) {
          throw new RuntimeException("Instance of nl.systemsbiology.semantics.sapp.domain.impl.ProdigalImpl expected");
        }
      }
      return (Prodigal)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/rbsMotif");
    this.checkCardMin1("http://gbol.life/0.1/gcContent");
    this.checkCardMin1("http://gbol.life/0.1/conf");
    this.checkCardMin1("http://gbol.life/0.1/rScore");
    this.checkCardMin1("http://gbol.life/0.1/score");
    this.checkCardMin1("http://gbol.life/0.1/sScore");
    this.checkCardMin1("http://gbol.life/0.1/cScore");
    this.checkCardMin1("http://gbol.life/0.1/tScore");
    this.checkCardMin1("http://gbol.life/0.1/startType");
    this.checkCardMin1("http://gbol.life/0.1/rbsSpacer");
    this.checkCardMin1("http://gbol.life/0.1/uScore");
  }

  public String getRbsMotif() {
    return this.getStringLit("http://gbol.life/0.1/rbsMotif",false);
  }

  public void setRbsMotif(String val) {
    this.setStringLit("http://gbol.life/0.1/rbsMotif",val);
  }

  public String getGcContent() {
    return this.getStringLit("http://gbol.life/0.1/gcContent",false);
  }

  public void setGcContent(String val) {
    this.setStringLit("http://gbol.life/0.1/gcContent",val);
  }

  public Float getConf() {
    return this.getFloatLit("http://gbol.life/0.1/conf",false);
  }

  public void setConf(Float val) {
    this.setFloatLit("http://gbol.life/0.1/conf",val);
  }

  public Float getRScore() {
    return this.getFloatLit("http://gbol.life/0.1/rScore",false);
  }

  public void setRScore(Float val) {
    this.setFloatLit("http://gbol.life/0.1/rScore",val);
  }

  public Float getScore() {
    return this.getFloatLit("http://gbol.life/0.1/score",false);
  }

  public void setScore(Float val) {
    this.setFloatLit("http://gbol.life/0.1/score",val);
  }

  public Float getSScore() {
    return this.getFloatLit("http://gbol.life/0.1/sScore",false);
  }

  public void setSScore(Float val) {
    this.setFloatLit("http://gbol.life/0.1/sScore",val);
  }

  public Float getCScore() {
    return this.getFloatLit("http://gbol.life/0.1/cScore",false);
  }

  public void setCScore(Float val) {
    this.setFloatLit("http://gbol.life/0.1/cScore",val);
  }

  public Float getTScore() {
    return this.getFloatLit("http://gbol.life/0.1/tScore",false);
  }

  public void setTScore(Float val) {
    this.setFloatLit("http://gbol.life/0.1/tScore",val);
  }

  public String getStartType() {
    return this.getStringLit("http://gbol.life/0.1/startType",false);
  }

  public void setStartType(String val) {
    this.setStringLit("http://gbol.life/0.1/startType",val);
  }

  public String getRbsSpacer() {
    return this.getStringLit("http://gbol.life/0.1/rbsSpacer",false);
  }

  public void setRbsSpacer(String val) {
    this.setStringLit("http://gbol.life/0.1/rbsSpacer",val);
  }

  public Float getUScore() {
    return this.getFloatLit("http://gbol.life/0.1/uScore",false);
  }

  public void setUScore(Float val) {
    this.setFloatLit("http://gbol.life/0.1/uScore",val);
  }

  public void remReference(Document val) {
    this.remRef("http://gbol.life/0.1/reference",val,true);
  }

  public List<? extends Document> getAllReference() {
    return this.getRefSet("http://gbol.life/0.1/reference",true,Document.class);
  }

  public void addReference(Document val) {
    this.addRef("http://gbol.life/0.1/reference",val);
  }

  public void remDerivedFrom(Feature val) {
    this.remRef("http://gbol.life/0.1/derivedFrom",val,true);
  }

  public List<? extends Feature> getAllDerivedFrom() {
    return this.getRefSet("http://gbol.life/0.1/derivedFrom",true,Feature.class);
  }

  public void addDerivedFrom(Feature val) {
    this.addRef("http://gbol.life/0.1/derivedFrom",val);
  }

  public String getProvenanceNote() {
    return this.getStringLit("http://gbol.life/0.1/provenanceNote",true);
  }

  public void setProvenanceNote(String val) {
    this.setStringLit("http://gbol.life/0.1/provenanceNote",val);
  }

  public String getExperiment() {
    return this.getStringLit("http://gbol.life/0.1/experiment",true);
  }

  public void setExperiment(String val) {
    this.setStringLit("http://gbol.life/0.1/experiment",val);
  }
}
