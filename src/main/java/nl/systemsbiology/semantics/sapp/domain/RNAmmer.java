package nl.systemsbiology.semantics.sapp.domain;

import java.lang.Double;
import java.lang.String;
import life.gbol.domain.ProvenanceAnnotation;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public interface RNAmmer extends ProvenanceAnnotation {
  Double getScore();

  void setScore(Double val);

  String getAttribute();

  void setAttribute(String val);
}
