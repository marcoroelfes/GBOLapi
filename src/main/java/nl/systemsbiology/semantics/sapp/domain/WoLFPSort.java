package nl.systemsbiology.semantics.sapp.domain;

import java.lang.Double;
import life.gbol.domain.ProvenanceAnnotation;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public interface WoLFPSort extends ProvenanceAnnotation {
  Double getScore();

  void setScore(Double val);
}
