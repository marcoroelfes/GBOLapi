package nl.systemsbiology.semantics.sapp.domain;

import java.lang.Double;
import java.lang.Integer;
import life.gbol.domain.Location;
import life.gbol.domain.ProvenanceAnnotation;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public interface HMM extends ProvenanceAnnotation {
  Double getTDievalue();

  void setTDievalue(Double val);

  Double getAccuracy();

  void setAccuracy(Double val);

  Double getTDbias();

  void setTDbias(Double val);

  Location getENVlocation();

  void setENVlocation(Location val);

  Double getFSevalue();

  void setFSevalue(Double val);

  Double getTDcevalue();

  void setTDcevalue(Double val);

  Double getTDscore();

  void setTDscore(Double val);

  Double getFSscore();

  void setFSscore(Double val);

  Integer getTDdomainNumber();

  void setTDdomainNumber(Integer val);

  Double getFSbias();

  void setFSbias(Double val);

  Location getALIlocation();

  void setALIlocation(Location val);

  Integer getTDdomainsTotal();

  void setTDdomainsTotal(Integer val);

  Location getHMMlocation();

  void setHMMlocation(Location val);
}
