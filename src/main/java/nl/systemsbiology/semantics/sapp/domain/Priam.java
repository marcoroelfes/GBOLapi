package nl.systemsbiology.semantics.sapp.domain;

import java.lang.Double;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.ProvenanceAnnotation;
import life.gbol.domain.Region;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public interface Priam extends ProvenanceAnnotation {
  String getProfileId();

  void setProfileId(String val);

  Region getProfileRegioin();

  void setProfileRegioin(Region val);

  Float getProfileProportion();

  void setProfileProportion(Float val);

  Double getEvalue();

  void setEvalue(Double val);

  Float getBitScore();

  void setBitScore(Float val);

  Region getSourceRegion();

  void setSourceRegion(Region val);

  String getQueryStrand();

  void setQueryStrand(String val);

  Float getPositiveHitProbability();

  void setPositiveHitProbability(Float val);

  String getFoundCatalyticPattern();

  void setFoundCatalyticPattern(String val);

  String getIsBestOverlap();

  void setIsBestOverlap(String val);

  Integer getProfileLength();

  void setProfileLength(Integer val);

  Integer getAlignLength();

  void setAlignLength(Integer val);
}
