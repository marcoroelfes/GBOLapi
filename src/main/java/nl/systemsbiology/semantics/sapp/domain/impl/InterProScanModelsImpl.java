package nl.systemsbiology.semantics.sapp.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Feature;
import life.gbol.domain.XRef;
import life.gbol.domain.impl.ProvenanceAnnotationImpl;
import nl.systemsbiology.semantics.sapp.domain.InterProScanModels;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public class InterProScanModelsImpl extends ProvenanceAnnotationImpl implements InterProScanModels {
  public static final String TypeIRI = "http://semantics.systemsbiology.nl/sapp/0.1/InterProScanModels";

  protected InterProScanModelsImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static InterProScanModels make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new InterProScanModelsImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,InterProScanModels.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,InterProScanModels.class,false);
          if(toRet == null) {
            toRet = new InterProScanModelsImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof InterProScanModels)) {
          throw new RuntimeException("Instance of nl.systemsbiology.semantics.sapp.domain.impl.InterProScanModelsImpl expected");
        }
      }
      return (InterProScanModels)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public String getKey() {
    return this.getStringLit("http://gbol.life/0.1/key",true);
  }

  public void setKey(String val) {
    this.setStringLit("http://gbol.life/0.1/key",val);
  }

  public String getName() {
    return this.getStringLit("http://gbol.life/0.1/name",true);
  }

  public void setName(String val) {
    this.setStringLit("http://gbol.life/0.1/name",val);
  }

  public String getDescription() {
    return this.getStringLit("http://gbol.life/0.1/description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("http://gbol.life/0.1/description",val);
  }

  public String getAccession() {
    return this.getStringLit("http://gbol.life/0.1/accession",true);
  }

  public void setAccession(String val) {
    this.setStringLit("http://gbol.life/0.1/accession",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life/0.1/xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life/0.1/xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life/0.1/xref",val);
  }

  public void remReference(Document val) {
    this.remRef("http://gbol.life/0.1/reference",val,true);
  }

  public List<? extends Document> getAllReference() {
    return this.getRefSet("http://gbol.life/0.1/reference",true,Document.class);
  }

  public void addReference(Document val) {
    this.addRef("http://gbol.life/0.1/reference",val);
  }

  public void remDerivedFrom(Feature val) {
    this.remRef("http://gbol.life/0.1/derivedFrom",val,true);
  }

  public List<? extends Feature> getAllDerivedFrom() {
    return this.getRefSet("http://gbol.life/0.1/derivedFrom",true,Feature.class);
  }

  public void addDerivedFrom(Feature val) {
    this.addRef("http://gbol.life/0.1/derivedFrom",val);
  }

  public String getProvenanceNote() {
    return this.getStringLit("http://gbol.life/0.1/provenanceNote",true);
  }

  public void setProvenanceNote(String val) {
    this.setStringLit("http://gbol.life/0.1/provenanceNote",val);
  }

  public String getExperiment() {
    return this.getStringLit("http://gbol.life/0.1/experiment",true);
  }

  public void setExperiment(String val) {
    this.setStringLit("http://gbol.life/0.1/experiment",val);
  }
}
