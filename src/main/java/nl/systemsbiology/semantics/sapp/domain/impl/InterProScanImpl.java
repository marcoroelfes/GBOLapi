package nl.systemsbiology.semantics.sapp.domain.impl;

import java.lang.Double;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Feature;
import life.gbol.domain.impl.ProvenanceAnnotationImpl;
import nl.systemsbiology.semantics.sapp.domain.InterProScan;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public class InterProScanImpl extends ProvenanceAnnotationImpl implements InterProScan {
  public static final String TypeIRI = "http://semantics.systemsbiology.nl/sapp/0.1/InterProScan";

  protected InterProScanImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static InterProScan make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new InterProScanImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,InterProScan.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,InterProScan.class,false);
          if(toRet == null) {
            toRet = new InterProScanImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof InterProScan)) {
          throw new RuntimeException("Instance of nl.systemsbiology.semantics.sapp.domain.impl.InterProScanImpl expected");
        }
      }
      return (InterProScan)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public Double getEvalue() {
    return this.getDoubleLit("http://gbol.life/0.1/evalue",true);
  }

  public void setEvalue(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/evalue",val);
  }

  public String getFamilyName() {
    return this.getStringLit("http://gbol.life/0.1/familyName",true);
  }

  public void setFamilyName(String val) {
    this.setStringLit("http://gbol.life/0.1/familyName",val);
  }

  public Double getScore() {
    return this.getDoubleLit("http://gbol.life/0.1/score",true);
  }

  public void setScore(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/score",val);
  }

  public void remReference(Document val) {
    this.remRef("http://gbol.life/0.1/reference",val,true);
  }

  public List<? extends Document> getAllReference() {
    return this.getRefSet("http://gbol.life/0.1/reference",true,Document.class);
  }

  public void addReference(Document val) {
    this.addRef("http://gbol.life/0.1/reference",val);
  }

  public void remDerivedFrom(Feature val) {
    this.remRef("http://gbol.life/0.1/derivedFrom",val,true);
  }

  public List<? extends Feature> getAllDerivedFrom() {
    return this.getRefSet("http://gbol.life/0.1/derivedFrom",true,Feature.class);
  }

  public void addDerivedFrom(Feature val) {
    this.addRef("http://gbol.life/0.1/derivedFrom",val);
  }

  public String getProvenanceNote() {
    return this.getStringLit("http://gbol.life/0.1/provenanceNote",true);
  }

  public void setProvenanceNote(String val) {
    this.setStringLit("http://gbol.life/0.1/provenanceNote",val);
  }

  public String getExperiment() {
    return this.getStringLit("http://gbol.life/0.1/experiment",true);
  }

  public void setExperiment(String val) {
    this.setStringLit("http://gbol.life/0.1/experiment",val);
  }
}
