package nl.systemsbiology.semantics.sapp.domain.impl;

import java.lang.Float;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Base;
import life.gbol.domain.Feature;
import life.gbol.domain.impl.ProvenanceAnnotationImpl;
import nl.systemsbiology.semantics.sapp.domain.SignalP;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public class SignalPImpl extends ProvenanceAnnotationImpl implements SignalP {
  public static final String TypeIRI = "http://semantics.systemsbiology.nl/sapp/0.1/SignalP";

  protected SignalPImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static SignalP make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new SignalPImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,SignalP.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,SignalP.class,false);
          if(toRet == null) {
            toRet = new SignalPImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof SignalP)) {
          throw new RuntimeException("Instance of nl.systemsbiology.semantics.sapp.domain.impl.SignalPImpl expected");
        }
      }
      return (SignalP)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/d");
    this.checkCardMin1("http://gbol.life/0.1/cMax");
    this.checkCardMin1("http://gbol.life/0.1/cPos");
    this.checkCardMin1("http://gbol.life/0.1/network");
    this.checkCardMin1("http://gbol.life/0.1/signal");
    this.checkCardMin1("http://gbol.life/0.1/dMaxCut");
    this.checkCardMin1("http://gbol.life/0.1/sMean");
    this.checkCardMin1("http://gbol.life/0.1/sMax");
    this.checkCardMin1("http://gbol.life/0.1/sPos");
    this.checkCardMin1("http://gbol.life/0.1/yMax");
    this.checkCardMin1("http://gbol.life/0.1/yPos");
  }

  public Float getD() {
    return this.getFloatLit("http://gbol.life/0.1/d",false);
  }

  public void setD(Float val) {
    this.setFloatLit("http://gbol.life/0.1/d",val);
  }

  public Float getCMax() {
    return this.getFloatLit("http://gbol.life/0.1/cMax",false);
  }

  public void setCMax(Float val) {
    this.setFloatLit("http://gbol.life/0.1/cMax",val);
  }

  public Base getCPos() {
    return this.getRef("http://gbol.life/0.1/cPos",false,Base.class);
  }

  public void setCPos(Base val) {
    this.setRef("http://gbol.life/0.1/cPos",val,Base.class);
  }

  public String getNetwork() {
    return this.getStringLit("http://gbol.life/0.1/network",false);
  }

  public void setNetwork(String val) {
    this.setStringLit("http://gbol.life/0.1/network",val);
  }

  public String getSignal() {
    return this.getStringLit("http://gbol.life/0.1/signal",false);
  }

  public void setSignal(String val) {
    this.setStringLit("http://gbol.life/0.1/signal",val);
  }

  public Float getDMaxCut() {
    return this.getFloatLit("http://gbol.life/0.1/dMaxCut",false);
  }

  public void setDMaxCut(Float val) {
    this.setFloatLit("http://gbol.life/0.1/dMaxCut",val);
  }

  public Float getSMean() {
    return this.getFloatLit("http://gbol.life/0.1/sMean",false);
  }

  public void setSMean(Float val) {
    this.setFloatLit("http://gbol.life/0.1/sMean",val);
  }

  public Float getSMax() {
    return this.getFloatLit("http://gbol.life/0.1/sMax",false);
  }

  public void setSMax(Float val) {
    this.setFloatLit("http://gbol.life/0.1/sMax",val);
  }

  public Base getSPos() {
    return this.getRef("http://gbol.life/0.1/sPos",false,Base.class);
  }

  public void setSPos(Base val) {
    this.setRef("http://gbol.life/0.1/sPos",val,Base.class);
  }

  public Float getYMax() {
    return this.getFloatLit("http://gbol.life/0.1/yMax",false);
  }

  public void setYMax(Float val) {
    this.setFloatLit("http://gbol.life/0.1/yMax",val);
  }

  public Base getYPos() {
    return this.getRef("http://gbol.life/0.1/yPos",false,Base.class);
  }

  public void setYPos(Base val) {
    this.setRef("http://gbol.life/0.1/yPos",val,Base.class);
  }

  public void remReference(Document val) {
    this.remRef("http://gbol.life/0.1/reference",val,true);
  }

  public List<? extends Document> getAllReference() {
    return this.getRefSet("http://gbol.life/0.1/reference",true,Document.class);
  }

  public void addReference(Document val) {
    this.addRef("http://gbol.life/0.1/reference",val);
  }

  public void remDerivedFrom(Feature val) {
    this.remRef("http://gbol.life/0.1/derivedFrom",val,true);
  }

  public List<? extends Feature> getAllDerivedFrom() {
    return this.getRefSet("http://gbol.life/0.1/derivedFrom",true,Feature.class);
  }

  public void addDerivedFrom(Feature val) {
    this.addRef("http://gbol.life/0.1/derivedFrom",val);
  }

  public String getProvenanceNote() {
    return this.getStringLit("http://gbol.life/0.1/provenanceNote",true);
  }

  public void setProvenanceNote(String val) {
    this.setStringLit("http://gbol.life/0.1/provenanceNote",val);
  }

  public String getExperiment() {
    return this.getStringLit("http://gbol.life/0.1/experiment",true);
  }

  public void setExperiment(String val) {
    this.setStringLit("http://gbol.life/0.1/experiment",val);
  }
}
