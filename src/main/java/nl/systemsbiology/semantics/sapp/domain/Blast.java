package nl.systemsbiology.semantics.sapp.domain;

import java.lang.Double;
import java.lang.Float;
import java.lang.Integer;
import life.gbol.domain.ProvenanceAnnotation;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public interface Blast extends ProvenanceAnnotation {
  Double getEvalue();

  void setEvalue(Double val);

  Integer getMismatches();

  void setMismatches(Integer val);

  Float getBitscore();

  void setBitscore(Float val);

  Integer getGaps();

  void setGaps(Integer val);

  Float getPercIdentity();

  void setPercIdentity(Float val);

  Integer getAlignmentLength();

  void setAlignmentLength(Integer val);
}
