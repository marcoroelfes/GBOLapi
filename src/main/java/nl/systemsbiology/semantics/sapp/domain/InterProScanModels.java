package nl.systemsbiology.semantics.sapp.domain;

import java.lang.String;
import java.util.List;
import life.gbol.domain.ProvenanceAnnotation;
import life.gbol.domain.XRef;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public interface InterProScanModels extends ProvenanceAnnotation {
  String getKey();

  void setKey(String val);

  String getName();

  void setName(String val);

  String getDescription();

  void setDescription(String val);

  String getAccession();

  void setAccession(String val);

  void remXref(XRef val);

  List<? extends XRef> getAllXref();

  void addXref(XRef val);
}
