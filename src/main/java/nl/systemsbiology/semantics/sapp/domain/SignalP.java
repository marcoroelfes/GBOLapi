package nl.systemsbiology.semantics.sapp.domain;

import java.lang.Float;
import java.lang.String;
import life.gbol.domain.Base;
import life.gbol.domain.ProvenanceAnnotation;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public interface SignalP extends ProvenanceAnnotation {
  Float getD();

  void setD(Float val);

  Float getCMax();

  void setCMax(Float val);

  Base getCPos();

  void setCPos(Base val);

  String getNetwork();

  void setNetwork(String val);

  String getSignal();

  void setSignal(String val);

  Float getDMaxCut();

  void setDMaxCut(Float val);

  Float getSMean();

  void setSMean(Float val);

  Float getSMax();

  void setSMax(Float val);

  Base getSPos();

  void setSPos(Base val);

  Float getYMax();

  void setYMax(Float val);

  Base getYPos();

  void setYPos(Base val);
}
