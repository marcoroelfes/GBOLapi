package nl.systemsbiology.semantics.sapp.domain.impl;

import java.lang.Double;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Feature;
import life.gbol.domain.Location;
import life.gbol.domain.impl.ProvenanceAnnotationImpl;
import nl.systemsbiology.semantics.sapp.domain.HMM;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public class HMMImpl extends ProvenanceAnnotationImpl implements HMM {
  public static final String TypeIRI = "http://semantics.systemsbiology.nl/sapp/0.1/HMM";

  protected HMMImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static HMM make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new HMMImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,HMM.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,HMM.class,false);
          if(toRet == null) {
            toRet = new HMMImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof HMM)) {
          throw new RuntimeException("Instance of nl.systemsbiology.semantics.sapp.domain.impl.HMMImpl expected");
        }
      }
      return (HMM)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/TDievalue");
    this.checkCardMin1("http://gbol.life/0.1/accuracy");
    this.checkCardMin1("http://gbol.life/0.1/TDbias");
    this.checkCardMin1("http://gbol.life/0.1/ENVlocation");
    this.checkCardMin1("http://gbol.life/0.1/FSevalue");
    this.checkCardMin1("http://gbol.life/0.1/TDcevalue");
    this.checkCardMin1("http://gbol.life/0.1/TDscore");
    this.checkCardMin1("http://gbol.life/0.1/FSscore");
    this.checkCardMin1("http://gbol.life/0.1/TDdomainNumber");
    this.checkCardMin1("http://gbol.life/0.1/FSbias");
    this.checkCardMin1("http://gbol.life/0.1/ALIlocation");
    this.checkCardMin1("http://gbol.life/0.1/TDdomainsTotal");
    this.checkCardMin1("http://gbol.life/0.1/HMMlocation");
  }

  public Double getTDievalue() {
    return this.getDoubleLit("http://gbol.life/0.1/TDievalue",false);
  }

  public void setTDievalue(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/TDievalue",val);
  }

  public Double getAccuracy() {
    return this.getDoubleLit("http://gbol.life/0.1/accuracy",false);
  }

  public void setAccuracy(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/accuracy",val);
  }

  public Double getTDbias() {
    return this.getDoubleLit("http://gbol.life/0.1/TDbias",false);
  }

  public void setTDbias(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/TDbias",val);
  }

  public Location getENVlocation() {
    return this.getRef("http://gbol.life/0.1/ENVlocation",false,Location.class);
  }

  public void setENVlocation(Location val) {
    this.setRef("http://gbol.life/0.1/ENVlocation",val,Location.class);
  }

  public Double getFSevalue() {
    return this.getDoubleLit("http://gbol.life/0.1/FSevalue",false);
  }

  public void setFSevalue(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/FSevalue",val);
  }

  public Double getTDcevalue() {
    return this.getDoubleLit("http://gbol.life/0.1/TDcevalue",false);
  }

  public void setTDcevalue(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/TDcevalue",val);
  }

  public Double getTDscore() {
    return this.getDoubleLit("http://gbol.life/0.1/TDscore",false);
  }

  public void setTDscore(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/TDscore",val);
  }

  public Double getFSscore() {
    return this.getDoubleLit("http://gbol.life/0.1/FSscore",false);
  }

  public void setFSscore(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/FSscore",val);
  }

  public Integer getTDdomainNumber() {
    return this.getIntegerLit("http://gbol.life/0.1/TDdomainNumber",false);
  }

  public void setTDdomainNumber(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/TDdomainNumber",val);
  }

  public Double getFSbias() {
    return this.getDoubleLit("http://gbol.life/0.1/FSbias",false);
  }

  public void setFSbias(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/FSbias",val);
  }

  public Location getALIlocation() {
    return this.getRef("http://gbol.life/0.1/ALIlocation",false,Location.class);
  }

  public void setALIlocation(Location val) {
    this.setRef("http://gbol.life/0.1/ALIlocation",val,Location.class);
  }

  public Integer getTDdomainsTotal() {
    return this.getIntegerLit("http://gbol.life/0.1/TDdomainsTotal",false);
  }

  public void setTDdomainsTotal(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/TDdomainsTotal",val);
  }

  public Location getHMMlocation() {
    return this.getRef("http://gbol.life/0.1/HMMlocation",false,Location.class);
  }

  public void setHMMlocation(Location val) {
    this.setRef("http://gbol.life/0.1/HMMlocation",val,Location.class);
  }

  public void remReference(Document val) {
    this.remRef("http://gbol.life/0.1/reference",val,true);
  }

  public List<? extends Document> getAllReference() {
    return this.getRefSet("http://gbol.life/0.1/reference",true,Document.class);
  }

  public void addReference(Document val) {
    this.addRef("http://gbol.life/0.1/reference",val);
  }

  public void remDerivedFrom(Feature val) {
    this.remRef("http://gbol.life/0.1/derivedFrom",val,true);
  }

  public List<? extends Feature> getAllDerivedFrom() {
    return this.getRefSet("http://gbol.life/0.1/derivedFrom",true,Feature.class);
  }

  public void addDerivedFrom(Feature val) {
    this.addRef("http://gbol.life/0.1/derivedFrom",val);
  }

  public String getProvenanceNote() {
    return this.getStringLit("http://gbol.life/0.1/provenanceNote",true);
  }

  public void setProvenanceNote(String val) {
    this.setStringLit("http://gbol.life/0.1/provenanceNote",val);
  }

  public String getExperiment() {
    return this.getStringLit("http://gbol.life/0.1/experiment",true);
  }

  public void setExperiment(String val) {
    this.setStringLit("http://gbol.life/0.1/experiment",val);
  }
}
