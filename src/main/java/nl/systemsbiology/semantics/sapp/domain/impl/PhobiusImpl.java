package nl.systemsbiology.semantics.sapp.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Feature;
import life.gbol.domain.Location;
import life.gbol.domain.impl.ProvenanceAnnotationImpl;
import nl.systemsbiology.semantics.sapp.domain.Phobius;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public class PhobiusImpl extends ProvenanceAnnotationImpl implements Phobius {
  public static final String TypeIRI = "http://semantics.systemsbiology.nl/sapp/0.1/Phobius";

  protected PhobiusImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Phobius make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PhobiusImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Phobius.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Phobius.class,false);
          if(toRet == null) {
            toRet = new PhobiusImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Phobius)) {
          throw new RuntimeException("Instance of nl.systemsbiology.semantics.sapp.domain.impl.PhobiusImpl expected");
        }
      }
      return (Phobius)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/location");
    this.checkCardMin1("http://gbol.life/0.1/type");
    this.checkCardMin1("http://gbol.life/0.1/description");
  }

  public Location getLocation() {
    return this.getRef("http://gbol.life/0.1/location",false,Location.class);
  }

  public void setLocation(Location val) {
    this.setRef("http://gbol.life/0.1/location",val,Location.class);
  }

  public String getType() {
    return this.getStringLit("http://gbol.life/0.1/type",false);
  }

  public void setType(String val) {
    this.setStringLit("http://gbol.life/0.1/type",val);
  }

  public String getDescription() {
    return this.getStringLit("http://gbol.life/0.1/description",false);
  }

  public void setDescription(String val) {
    this.setStringLit("http://gbol.life/0.1/description",val);
  }

  public void remReference(Document val) {
    this.remRef("http://gbol.life/0.1/reference",val,true);
  }

  public List<? extends Document> getAllReference() {
    return this.getRefSet("http://gbol.life/0.1/reference",true,Document.class);
  }

  public void addReference(Document val) {
    this.addRef("http://gbol.life/0.1/reference",val);
  }

  public void remDerivedFrom(Feature val) {
    this.remRef("http://gbol.life/0.1/derivedFrom",val,true);
  }

  public List<? extends Feature> getAllDerivedFrom() {
    return this.getRefSet("http://gbol.life/0.1/derivedFrom",true,Feature.class);
  }

  public void addDerivedFrom(Feature val) {
    this.addRef("http://gbol.life/0.1/derivedFrom",val);
  }

  public String getProvenanceNote() {
    return this.getStringLit("http://gbol.life/0.1/provenanceNote",true);
  }

  public void setProvenanceNote(String val) {
    this.setStringLit("http://gbol.life/0.1/provenanceNote",val);
  }

  public String getExperiment() {
    return this.getStringLit("http://gbol.life/0.1/experiment",true);
  }

  public void setExperiment(String val) {
    this.setStringLit("http://gbol.life/0.1/experiment",val);
  }
}
