package nl.systemsbiology.semantics.sapp.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Feature;
import life.gbol.domain.Location;
import life.gbol.domain.impl.ProvenanceAnnotationImpl;
import nl.systemsbiology.semantics.sapp.domain.TMHMM;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public class TMHMMImpl extends ProvenanceAnnotationImpl implements TMHMM {
  public static final String TypeIRI = "http://semantics.systemsbiology.nl/sapp/0.1/TMHMM";

  protected TMHMMImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static TMHMM make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new TMHMMImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,TMHMM.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,TMHMM.class,false);
          if(toRet == null) {
            toRet = new TMHMMImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof TMHMM)) {
          throw new RuntimeException("Instance of nl.systemsbiology.semantics.sapp.domain.impl.TMHMMImpl expected");
        }
      }
      return (TMHMM)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public Location getLocation() {
    return this.getRef("http://gbol.life/0.1/location",true,Location.class);
  }

  public void setLocation(Location val) {
    this.setRef("http://gbol.life/0.1/location",val,Location.class);
  }

  public String getType() {
    return this.getStringLit("http://gbol.life/0.1/type",true);
  }

  public void setType(String val) {
    this.setStringLit("http://gbol.life/0.1/type",val);
  }

  public String getSequence() {
    return this.getStringLit("http://gbol.life/0.1/sequence",true);
  }

  public void setSequence(String val) {
    this.setStringLit("http://gbol.life/0.1/sequence",val);
  }

  public void remReference(Document val) {
    this.remRef("http://gbol.life/0.1/reference",val,true);
  }

  public List<? extends Document> getAllReference() {
    return this.getRefSet("http://gbol.life/0.1/reference",true,Document.class);
  }

  public void addReference(Document val) {
    this.addRef("http://gbol.life/0.1/reference",val);
  }

  public void remDerivedFrom(Feature val) {
    this.remRef("http://gbol.life/0.1/derivedFrom",val,true);
  }

  public List<? extends Feature> getAllDerivedFrom() {
    return this.getRefSet("http://gbol.life/0.1/derivedFrom",true,Feature.class);
  }

  public void addDerivedFrom(Feature val) {
    this.addRef("http://gbol.life/0.1/derivedFrom",val);
  }

  public String getProvenanceNote() {
    return this.getStringLit("http://gbol.life/0.1/provenanceNote",true);
  }

  public void setProvenanceNote(String val) {
    this.setStringLit("http://gbol.life/0.1/provenanceNote",val);
  }

  public String getExperiment() {
    return this.getStringLit("http://gbol.life/0.1/experiment",true);
  }

  public void setExperiment(String val) {
    this.setStringLit("http://gbol.life/0.1/experiment",val);
  }
}
