package nl.systemsbiology.semantics.sapp.domain;

import java.lang.String;
import life.gbol.domain.Location;
import life.gbol.domain.ProvenanceAnnotation;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public interface TMHMM extends ProvenanceAnnotation {
  Location getLocation();

  void setLocation(Location val);

  String getType();

  void setType(String val);

  String getSequence();

  void setSequence(String val);
}
