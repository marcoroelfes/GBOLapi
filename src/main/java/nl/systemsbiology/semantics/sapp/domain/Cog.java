package nl.systemsbiology.semantics.sapp.domain;

import java.lang.String;

/**
 * Code generated from http://semantics.systemsbiology.nl/sapp/0.1/ ontology
 */
public interface Cog extends Blast {
  String getLetter();

  void setLetter(String val);

  String getMembershipClass();

  void setMembershipClass(String val);

  String getLabel();

  void setLabel(String val);

  String getOrganismName();

  void setOrganismName(String val);
}
