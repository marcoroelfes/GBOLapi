package org.purl.ontology.bibo.domain;

/**
 * Code generated from http://purl.org/ontology/bibo/ ontology
 */
public interface Thesis extends Document {
  ThesisDegree getDegree();

  void setDegree(ThesisDegree val);
}
