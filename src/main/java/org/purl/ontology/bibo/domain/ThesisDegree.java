package org.purl.ontology.bibo.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://purl.org/ontology/bibo/ ontology
 */
public enum ThesisDegree implements EnumClass {
  ms("http://purl.org/ontology/bibo/degrees/ms",new ThesisDegree[]{}),

  phd("http://purl.org/ontology/bibo/degrees/phd",new ThesisDegree[]{}),

  ma("http://purl.org/ontology/bibo/degrees/ma",new ThesisDegree[]{});

  private ThesisDegree[] parents;

  private String iri;

  private ThesisDegree(String iri, ThesisDegree[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static ThesisDegree make(String iri) {
    for(ThesisDegree item : ThesisDegree.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
