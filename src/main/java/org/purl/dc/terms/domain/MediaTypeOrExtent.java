package org.purl.dc.terms.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://purl.org/dc/terms/ ontology
 */
public interface MediaTypeOrExtent extends OWLThing {
  String getValue();

  void setValue(String val);

  String getLabel();

  void setLabel(String val);
}
