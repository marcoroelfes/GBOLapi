package org.rdfs.ns._void.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.rdfs.ns._void.domain.Dataset;

/**
 * Code generated from http://rdfs.org/ns/void# ontology
 */
public class DatasetImpl extends OWLThingImpl implements Dataset {
  public static final String TypeIRI = "http://rdfs.org/ns/void#Dataset";

  protected DatasetImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Dataset make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new DatasetImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Dataset.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Dataset.class,false);
          if(toRet == null) {
            toRet = new DatasetImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Dataset)) {
          throw new RuntimeException("Instance of org.rdfs.ns._void.domain.impl.DatasetImpl expected");
        }
      }
      return (Dataset)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://purl.org/dc/terms/hasVersion");
  }

  public String getLicense() {
    return this.getExternalRef("http://purl.org/dc/terms/license",true);
  }

  public void setLicense(String val) {
    this.setExternalRef("http://purl.org/dc/terms/license",val);
  }

  public String getHasVersion() {
    return this.getStringLit("http://purl.org/dc/terms/hasVersion",false);
  }

  public void setHasVersion(String val) {
    this.setStringLit("http://purl.org/dc/terms/hasVersion",val);
  }

  public String getUriSpace() {
    return this.getExternalRef("http://rdfs.org/ns/void#uriSpace",true);
  }

  public void setUriSpace(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#uriSpace",val);
  }

  public String getUriRegexPattern() {
    return this.getStringLit("http://rdfs.org/ns/void#uriRegexPattern",true);
  }

  public void setUriRegexPattern(String val) {
    this.setStringLit("http://rdfs.org/ns/void#uriRegexPattern",val);
  }

  public String getWaiver() {
    return this.getStringLit("http://vocab.org/waiver/terms/normswaiver",true);
  }

  public void setWaiver(String val) {
    this.setStringLit("http://vocab.org/waiver/terms/normswaiver",val);
  }

  public String getNorms() {
    return this.getExternalRef("http://vocab.org/waiver/terms/normsnorms",true);
  }

  public void setNorms(String val) {
    this.setExternalRef("http://vocab.org/waiver/terms/normsnorms",val);
  }

  public void remLanguage(String val) {
    this.remStringLit("http://purl.org/dc/terms/language",val,true);
  }

  public List<? extends String> getAllLanguage() {
    return this.getStringLitSet("http://purl.org/dc/terms/language",true);
  }

  public void addLanguage(String val) {
    this.addStringLit("http://purl.org/dc/terms/language",val);
  }

  public String getDescription() {
    return this.getStringLit("http://purl.org/dc/terms/description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("http://purl.org/dc/terms/description",val);
  }

  public String getUriLookupEndpoint() {
    return this.getExternalRef("http://rdfs.org/ns/void#uriLookupEndpoint",true);
  }

  public void setUriLookupEndpoint(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#uriLookupEndpoint",val);
  }

  public String getSparqlEndpoint() {
    return this.getExternalRef("http://rdfs.org/ns/void#sparqlEndpoint",true);
  }

  public void setSparqlEndpoint(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#sparqlEndpoint",val);
  }

  public String getDataDump() {
    return this.getExternalRef("http://rdfs.org/ns/void#dataDump",true);
  }

  public void setDataDump(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#dataDump",val);
  }

  public String getComment() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#comment",true);
  }

  public void setComment(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#comment",val);
  }

  public String getTitle() {
    return this.getStringLit("http://purl.org/dc/terms/title",true);
  }

  public void setTitle(String val) {
    this.setStringLit("http://purl.org/dc/terms/title",val);
  }

  public String getLabel() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#label",true);
  }

  public void setLabel(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#label",val);
  }
}
