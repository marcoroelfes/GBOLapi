package org.w3.rdfsyntaxns.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://www.w3.org/1999/02/22-rdf-syntax-ns# ontology
 */
public interface List extends OWLThing {
}
