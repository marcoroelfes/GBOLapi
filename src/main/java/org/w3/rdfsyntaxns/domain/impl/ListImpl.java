package org.w3.rdfsyntaxns.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.w3.rdfsyntaxns.domain.List;

/**
 * Code generated from http://www.w3.org/1999/02/22-rdf-syntax-ns# ontology
 */
public class ListImpl extends OWLThingImpl implements List {
  public static final String TypeIRI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#List";

  protected ListImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static List make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ListImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,List.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,List.class,false);
          if(toRet == null) {
            toRet = new ListImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof List)) {
          throw new RuntimeException("Instance of org.w3.rdfsyntaxns.domain.impl.ListImpl expected");
        }
      }
      return (List)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
