package org.w3.ns.prov.domain;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public interface Person extends Agent, com.xmlns.foaf.domain.Person {
}
