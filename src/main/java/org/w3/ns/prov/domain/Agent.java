package org.w3.ns.prov.domain;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public interface Agent extends com.xmlns.foaf.domain.Agent {
  Agent getActedOnBehalfOf();

  void setActedOnBehalfOf(Agent val);
}
