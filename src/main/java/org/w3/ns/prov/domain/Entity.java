package org.w3.ns.prov.domain;

import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public interface Entity extends OWLThing {
  Activity getWasGeneratedBy();

  void setWasGeneratedBy(Activity val);

  Agent getWasAttributedTo();

  void setWasAttributedTo(Agent val);

  void remWasDerivedFrom(Entity val);

  List<? extends Entity> getAllWasDerivedFrom();

  void addWasDerivedFrom(Entity val);
}
