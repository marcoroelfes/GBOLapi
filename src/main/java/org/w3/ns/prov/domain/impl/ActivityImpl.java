package org.w3.ns.prov.domain.impl;

import java.lang.String;
import java.time.LocalDateTime;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Activity;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public class ActivityImpl extends OWLThingImpl implements Activity {
  public static final String TypeIRI = "http://www.w3.org/ns/prov#Activity";

  protected ActivityImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Activity make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ActivityImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Activity.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Activity.class,false);
          if(toRet == null) {
            toRet = new ActivityImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Activity)) {
          throw new RuntimeException("Instance of org.w3.ns.prov.domain.impl.ActivityImpl expected");
        }
      }
      return (Activity)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public LocalDateTime getStartedAtTime() {
    return this.getDateTimeLit("http://www.w3.org/ns/prov#startedAtTime",true);
  }

  public void setStartedAtTime(LocalDateTime val) {
    this.setDateTimeLit("http://www.w3.org/ns/prov#startedAtTime",val);
  }

  public Agent getWasAssociatedWith() {
    return this.getRef("http://gbol.life/0.1/wasAssociatedWith",true,Agent.class);
  }

  public void setWasAssociatedWith(Agent val) {
    this.setRef("http://gbol.life/0.1/wasAssociatedWith",val,Agent.class);
  }

  public LocalDateTime getEndedAtTime() {
    return this.getDateTimeLit("http://www.w3.org/ns/prov#endedAtTime",true);
  }

  public void setEndedAtTime(LocalDateTime val) {
    this.setDateTimeLit("http://www.w3.org/ns/prov#endedAtTime",val);
  }

  public void remUsed(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#used",val,true);
  }

  public List<? extends Entity> getAllUsed() {
    return this.getRefSet("http://www.w3.org/ns/prov#used",true,Entity.class);
  }

  public void addUsed(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#used",val);
  }

  public void remGenerated(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#generated",val,true);
  }

  public List<? extends Entity> getAllGenerated() {
    return this.getRefSet("http://www.w3.org/ns/prov#generated",true,Entity.class);
  }

  public void addGenerated(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#generated",val);
  }

  public Activity getWasInformedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasInformedBy",true,Activity.class);
  }

  public void setWasInformedBy(Activity val) {
    this.setRef("http://www.w3.org/ns/prov#wasInformedBy",val,Activity.class);
  }
}
