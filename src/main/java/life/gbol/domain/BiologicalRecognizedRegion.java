package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface BiologicalRecognizedRegion extends RecognizedRegion {
  String getBoundMoiety();

  void setBoundMoiety(String val);
}
