package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface VariationFeature extends SequenceAnnotation {
  String getReplace();

  void setReplace(String val);
}
