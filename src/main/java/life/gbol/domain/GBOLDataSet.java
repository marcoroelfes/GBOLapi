package life.gbol.domain;

import java.lang.Integer;
import java.lang.String;
import java.util.List;
import org.rdfs.ns._void.domain.Dataset;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface GBOLDataSet extends Entity, Dataset {
  void remReferences(Citation val);

  List<? extends Citation> getAllReferences();

  void addReferences(Citation val);

  EntryType getEntryType();

  void setEntryType(EntryType val);

  void remSequences(Sequence val);

  List<? extends Sequence> getAllSequences();

  void addSequences(Sequence val);

  void remLinkedDataBases(Database val);

  List<? extends Database> getAllLinkedDataBases();

  void addLinkedDataBases(Database val);

  void remSamples(Sample val);

  List<? extends Sample> getAllSamples();

  void addSamples(Sample val);

  Integer getDataSetVersion();

  void setDataSetVersion(Integer val);

  void remAnnotationResults(AnnotationResult val);

  List<? extends AnnotationResult> getAllAnnotationResults();

  void addAnnotationResults(AnnotationResult val);

  void remOrganisms(Organism val);

  List<? extends Organism> getAllOrganisms();

  void addOrganisms(Organism val);

  void remKeywords(String val);

  List<? extends String> getAllKeywords();

  void addKeywords(String val);

  void remXref(XRef val);

  List<? extends XRef> getAllXref();

  void addXref(XRef val);

  void remNote(Note val);

  List<? extends Note> getAllNote();

  void addNote(Note val);
}
