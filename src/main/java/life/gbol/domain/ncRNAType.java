package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum ncRNAType implements EnumClass {
  antisenseRNA("http://gbol.life/0.1/antisenseRNA",new ncRNAType[]{}),

  miRNA("http://gbol.life/0.1/miRNA",new ncRNAType[]{}),

  SRPRNA("http://gbol.life/0.1/SRPRNA",new ncRNAType[]{}),

  scRNA("http://gbol.life/0.1/scRNA",new ncRNAType[]{}),

  ncRNATypeOther("http://gbol.life/0.1/ncRNATypeOther",new ncRNAType[]{}),

  piRNA("http://gbol.life/0.1/piRNA",new ncRNAType[]{}),

  snRNA("http://gbol.life/0.1/snRNA",new ncRNAType[]{}),

  YRNA("http://gbol.life/0.1/YRNA",new ncRNAType[]{}),

  ribozyme("http://gbol.life/0.1/ribozyme",new ncRNAType[]{}),

  snoRNA("http://gbol.life/0.1/snoRNA",new ncRNAType[]{}),

  guideRNA("http://gbol.life/0.1/guideRNA",new ncRNAType[]{}),

  rasiRNA("http://gbol.life/0.1/rasiRNA",new ncRNAType[]{}),

  siRNA("http://gbol.life/0.1/siRNA",new ncRNAType[]{}),

  lncRNA("http://gbol.life/0.1/lncRNA",new ncRNAType[]{}),

  vaultRNA("http://gbol.life/0.1/vaultRNA",new ncRNAType[]{}),

  telomeraseRNA("http://gbol.life/0.1/telomeraseRNA",new ncRNAType[]{}),

  autocatalyticallySplicedIntron("http://gbol.life/0.1/autocatalyticallySplicedIntron",new ncRNAType[]{}),

  hammerheadRibozyme("http://gbol.life/0.1/hammerheadRibozyme",new ncRNAType[]{}),

  RNasePRNA("http://gbol.life/0.1/RNasePRNA",new ncRNAType[]{}),

  RNaseMRPRNA("http://gbol.life/0.1/RNaseMRPRNA",new ncRNAType[]{});

  private ncRNAType[] parents;

  private String iri;

  private ncRNAType(String iri, ncRNAType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static ncRNAType make(String iri) {
    for(ncRNAType item : ncRNAType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
