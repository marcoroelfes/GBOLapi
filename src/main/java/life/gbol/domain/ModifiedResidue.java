package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ModifiedResidue extends ProteinFeature {
  String getModifiedState();

  void setModifiedState(String val);

  String getResidue();

  void setResidue(String val);

  String getUnmodifiedState();

  void setUnmodifiedState(String val);

  String getModificationFunction();

  void setModificationFunction(String val);
}
