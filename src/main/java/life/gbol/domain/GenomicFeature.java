package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface GenomicFeature extends NAFeature {
  Operon getOperon();

  void setOperon(Operon val);
}
