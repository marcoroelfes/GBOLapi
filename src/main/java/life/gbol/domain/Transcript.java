package life.gbol.domain;

import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Transcript extends NASequence {
  ExonList getExonList();

  void setExonList(ExonList val);

  void remFeature(TranscriptFeature val);

  List<? extends TranscriptFeature> getAllFeature();

  void addFeature(TranscriptFeature val);
}
