package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface IntegratedVirus extends TranscriptionElement {
  Location getIntegratedRegion();

  void setIntegratedRegion(Location val);

  NASequence getVirusGenome();

  void setVirusGenome(NASequence val);
}
