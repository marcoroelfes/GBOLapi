package life.gbol.domain;

import java.time.LocalDateTime;
import org.w3.ns.prov.domain.Activity;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface AnnotationActivity extends Activity {
  LocalDateTime getStartedAtTime();

  void setStartedAtTime(LocalDateTime val);

  LocalDateTime getEndedAtTime();

  void setEndedAtTime(LocalDateTime val);
}
