package life.gbol.domain;

import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Protein extends Sequence {
  void remFeature(ProteinFeature val);

  List<? extends ProteinFeature> getAllFeature();

  void addFeature(ProteinFeature val);
}
