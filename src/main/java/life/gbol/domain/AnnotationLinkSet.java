package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface AnnotationLinkSet extends AnnotationResult, GBOLLinkSet {
}
