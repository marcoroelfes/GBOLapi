package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Base extends Location {
  Position getAt();

  void setAt(Position val);

  StrandPosition getStrand();

  void setStrand(StrandPosition val);
}
