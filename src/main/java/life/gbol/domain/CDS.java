package life.gbol.domain;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface CDS extends TranscriptFeature {
  String getProteinId();

  void setProteinId(String val);

  Boolean getRibosomalSlippage();

  void setRibosomalSlippage(Boolean val);

  String getException();

  void setException(String val);

  String getProduct();

  void setProduct(String val);

  Integer getCodonStart();

  void setCodonStart(Integer val);

  Protein getProtein();

  void setProtein(Protein val);

  void remTranslExcept(TranslExcept val);

  List<? extends TranslExcept> getAllTranslExcept();

  void addTranslExcept(TranslExcept val);
}
