package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ReplicationOrigin extends BiologicalRecognizedRegion {
  Direction getReplicationDirection();

  void setReplicationDirection(Direction val);
}
