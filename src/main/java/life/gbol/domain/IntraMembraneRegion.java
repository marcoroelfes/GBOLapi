package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface IntraMembraneRegion extends ProteinFeature {
  String getNTerminalMembrane();

  void setNTerminalMembrane(String val);

  String getCTerminalMembrane();

  void setCTerminalMembrane(String val);
}
