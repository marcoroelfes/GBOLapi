package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum QTLTypes implements EnumClass {
  MetaboliteQTL("http://gbol.life/0.1/MetaboliteQTL",new QTLTypes[]{}),

  StandardQTL("http://gbol.life/0.1/StandardQTL",new QTLTypes[]{}),

  ExpressionQTL("http://gbol.life/0.1/ExpressionQTL",new QTLTypes[]{}),

  ProteinQTL("http://gbol.life/0.1/ProteinQTL",new QTLTypes[]{});

  private QTLTypes[] parents;

  private String iri;

  private QTLTypes(String iri, QTLTypes[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static QTLTypes make(String iri) {
    for(QTLTypes item : QTLTypes.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
