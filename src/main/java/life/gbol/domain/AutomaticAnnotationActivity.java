package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface AutomaticAnnotationActivity extends AnnotationActivity {
  AnnotationSoftware getWasAssociatedWith();

  void setWasAssociatedWith(AnnotationSoftware val);

  String getParameterString();

  void setParameterString(String val);
}
