package life.gbol.domain;

import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Scaffold extends UncompleteNASequence {
  void remSourceContigs(Contig val);

  List<? extends Contig> getAllSourceContigs();

  void addSourceContigs(Contig val);
}
