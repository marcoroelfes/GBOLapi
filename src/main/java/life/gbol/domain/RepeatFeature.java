package life.gbol.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface RepeatFeature extends GenomicFeature {
  String getRptUnitSeq();

  void setRptUnitSeq(String val);

  void remSatellite(Satellite val);

  List<? extends Satellite> getAllSatellite();

  void addSatellite(Satellite val);

  RepeatType getRptType();

  void setRptType(RepeatType val);

  String getRptFamily();

  void setRptFamily(String val);

  Region getRptUnitRange();

  void setRptUnitRange(Region val);
}
