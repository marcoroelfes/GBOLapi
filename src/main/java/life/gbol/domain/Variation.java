package life.gbol.domain;

import java.lang.Double;
import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Variation extends VariationFeature {
  String getReference();

  void setReference(String val);

  Double getAllele_freq();

  void setAllele_freq(Double val);

  String getAlternate();

  void setAlternate(String val);

  String getInfo();

  void setInfo(String val);

  String getID();

  void setID(String val);

  String getQuality();

  void setQuality(String val);
}
