package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Satellite extends OWLThing {
  String getSatelliteName();

  void setSatelliteName(String val);

  SatelliteType getSatelliteType();

  void setSatelliteType(SatelliteType val);
}
