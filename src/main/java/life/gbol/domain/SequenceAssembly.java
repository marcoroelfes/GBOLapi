package life.gbol.domain;

import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface SequenceAssembly extends OWLThing {
  Provenance getProvenance();

  void setProvenance(Provenance val);

  SequencePart getSequencePart(int index);

  List<? extends SequencePart> getAllSequencePart();

  void addSequencePart(SequencePart val);

  void setSequencePart(SequencePart val, int index);

  void remSequencePart(SequencePart val);
}
