package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface PrimerBinding extends ArtificialRecognizedRegion {
  String getPCRConditions();

  void setPCRConditions(String val);
}
