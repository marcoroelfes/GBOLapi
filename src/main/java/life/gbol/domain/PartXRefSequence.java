package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface PartXRefSequence extends PartSequence {
  String getIncludedXRefVersion();

  void setIncludedXRefVersion(String val);

  XRef getIncludedXRefSequence();

  void setIncludedXRefSequence(XRef val);
}
