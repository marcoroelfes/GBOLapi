package life.gbol.domain;

import java.lang.Integer;
import java.lang.String;
import java.time.LocalDate;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface PublishedGBOLDataSet extends GBOLDataSet {
  LocalDate getHolddate();

  void setHolddate(LocalDate val);

  LocalDate getLastPublishedDate();

  void setLastPublishedDate(LocalDate val);

  String getAbstract();

  void setAbstract(String val);

  String getReleaseStatus();

  void setReleaseStatus(String val);

  Integer getLastPublishedRelease();

  void setLastPublishedRelease(Integer val);

  void remDblinks(XRef val);

  List<? extends XRef> getAllDblinks();

  void addDblinks(XRef val);

  LocalDate getFirstPublic();

  void setFirstPublic(LocalDate val);

  Integer getFirstPublicRelease();

  void setFirstPublicRelease(Integer val);
}
