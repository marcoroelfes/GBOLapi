package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface NAFeature extends Feature {
  String getMap();

  void setMap(String val);
}
