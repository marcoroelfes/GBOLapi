package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum ProteinStructureType implements EnumClass {
  BetaStrand("http://gbol.life/0.1/BetaStrand",new ProteinStructureType[]{}),

  AlphaHelix("http://gbol.life/0.1/AlphaHelix",new ProteinStructureType[]{}),

  CoiledCoil("http://gbol.life/0.1/CoiledCoil",new ProteinStructureType[]{}),

  Turn("http://gbol.life/0.1/Turn",new ProteinStructureType[]{});

  private ProteinStructureType[] parents;

  private String iri;

  private ProteinStructureType(String iri, ProteinStructureType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static ProteinStructureType make(String iri) {
    for(ProteinStructureType item : ProteinStructureType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
