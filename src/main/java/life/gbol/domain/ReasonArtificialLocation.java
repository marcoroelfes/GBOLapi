package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum ReasonArtificialLocation implements EnumClass {
  HeterogeneousPopulation("http://gbol.life/0.1/HeterogeneousPopulation",new ReasonArtificialLocation[]{}),

  LowQualitySeqRegion("http://gbol.life/0.1/LowQualitySeqRegion",new ReasonArtificialLocation[]{});

  private ReasonArtificialLocation[] parents;

  private String iri;

  private ReasonArtificialLocation(String iri, ReasonArtificialLocation[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static ReasonArtificialLocation make(String iri) {
    for(ReasonArtificialLocation item : ReasonArtificialLocation.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
