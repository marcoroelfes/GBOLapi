package life.gbol.domain;

import java.lang.String;
import org.w3.ns.prov.domain.Person;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Curator extends Person {
  String getOrcid();

  void setOrcid(String val);
}
