package life.gbol.domain;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Feature extends OWLThing {
  Location getLocation();

  void setLocation(Location val);

  void remCitation(Citation val);

  List<? extends Citation> getAllCitation();

  void addCitation(Citation val);

  String getFunction();

  void setFunction(String val);

  String getPhenotype();

  void setPhenotype(String val);

  void remProvenance(FeatureProvenance val);

  List<? extends FeatureProvenance> getAllProvenance();

  void addProvenance(FeatureProvenance val);

  ReasonArtificialLocation getArtificialLocation();

  void setArtificialLocation(ReasonArtificialLocation val);

  void remAccession(String val);

  List<? extends String> getAllAccession();

  void addAccession(String val);

  String getStandardName();

  void setStandardName(String val);

  void remXref(XRef val);

  List<? extends XRef> getAllXref();

  void addXref(XRef val);

  void remNote(Note val);

  List<? extends Note> getAllNote();

  void addNote(Note val);
}
