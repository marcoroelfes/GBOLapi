package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Note extends Qualifier {
  Provenance getProvenance();

  void setProvenance(Provenance val);

  String getText();

  void setText(String val);
}
