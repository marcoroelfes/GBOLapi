package life.gbol.domain;

import java.lang.String;
import java.time.LocalDateTime;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface PublicDataFile extends DataFile {
  String getUrl();

  void setUrl(String val);

  LocalDateTime getAccessedOn();

  void setAccessedOn(LocalDateTime val);
}
