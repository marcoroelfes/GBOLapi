package life.gbol.domain;

import java.util.List;
import org.rdfs.ns._void.domain.Dataset;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface DataFile extends Entity, Dataset {
  void remContainedEntity(Entity val);

  List<? extends Entity> getAllContainedEntity();

  void addContainedEntity(Entity val);
}
