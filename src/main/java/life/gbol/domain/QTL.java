package life.gbol.domain;

import java.lang.Double;
import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface QTL extends Feature {
  String getComment();

  void setComment(String val);

  void remTerm(String val);

  List<? extends String> getAllTerm();

  void addTerm(String val);

  QTLTypes getType();

  void setType(QTLTypes val);

  String getLinkageGroup();

  void setLinkageGroup(String val);

  QTLMap getMap();

  void setMap(QTLMap val);

  String getTrait();

  void setTrait(String val);

  void remAssociatedFeature(Feature val);

  List<? extends Feature> getAllAssociatedFeature();

  void addAssociatedFeature(Feature val);

  String getSymbol();

  void setSymbol(String val);

  Double getRelativeStart();

  void setRelativeStart(Double val);

  Double getRelativeEnd();

  void setRelativeEnd(Double val);

  String getAccession();

  void setAccession(String val);
}
