package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ProteinInteractionSite extends BindingSide {
  ProteinInteractionSite getBindingProtein();

  void setBindingProtein(ProteinInteractionSite val);
}
