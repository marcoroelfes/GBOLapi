package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Taxon extends OWLThing {
  String getTaxonName();

  void setTaxonName(String val);

  Rank getTaxonRank();

  void setTaxonRank(Rank val);
}
