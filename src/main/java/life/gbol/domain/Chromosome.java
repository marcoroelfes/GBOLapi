package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Chromosome extends CompleteNASequence {
  String getChromosome();

  void setChromosome(String val);
}
