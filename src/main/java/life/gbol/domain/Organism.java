package life.gbol.domain;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Organism extends OWLThing {
  void remTaxonomyLineage(Taxon val);

  List<? extends Taxon> getAllTaxonomyLineage();

  void addTaxonomyLineage(Taxon val);

  TaxonomyRef getTaxonomy();

  void setTaxonomy(TaxonomyRef val);

  String getScientificName();

  void setScientificName(String val);
}
