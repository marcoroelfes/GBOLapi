package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface tmRNA extends MaturedRNA {
  Region getTagPeptide();

  void setTagPeptide(Region val);

  String getProduct();

  void setProduct(String val);
}
