package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AminoAcid;
import life.gbol.domain.AntiCodon;
import life.gbol.domain.Region;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class AntiCodonImpl extends OWLThingImpl implements AntiCodon {
  public static final String TypeIRI = "http://gbol.life/0.1/AntiCodon";

  protected AntiCodonImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static AntiCodon make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new AntiCodonImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,AntiCodon.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,AntiCodon.class,false);
          if(toRet == null) {
            toRet = new AntiCodonImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof AntiCodon)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.AntiCodonImpl expected");
        }
      }
      return (AntiCodon)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/antiCodonSequence");
    this.checkCardMin1("http://gbol.life/0.1/aminoAcid");
    this.checkCardMin1("http://gbol.life/0.1/antiCodingRegion");
  }

  public String getAntiCodonSequence() {
    return this.getStringLit("http://gbol.life/0.1/antiCodonSequence",false);
  }

  public void setAntiCodonSequence(String val) {
    this.setStringLit("http://gbol.life/0.1/antiCodonSequence",val);
  }

  public AminoAcid getAminoAcid() {
    return this.getEnum("http://gbol.life/0.1/aminoAcid",false,AminoAcid.class);
  }

  public void setAminoAcid(AminoAcid val) {
    this.setEnum("http://gbol.life/0.1/aminoAcid",val,AminoAcid.class);
  }

  public Region getAntiCodingRegion() {
    return this.getRef("http://gbol.life/0.1/antiCodingRegion",false,Region.class);
  }

  public void setAntiCodingRegion(Region val) {
    this.setRef("http://gbol.life/0.1/antiCodingRegion",val,Region.class);
  }
}
