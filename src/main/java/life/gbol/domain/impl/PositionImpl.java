package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Position;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class PositionImpl extends OWLThingImpl implements Position {
  public static final String TypeIRI = "http://gbol.life/0.1/Position";

  protected PositionImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Position make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PositionImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Position.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Position.class,false);
          if(toRet == null) {
            toRet = new PositionImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Position)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.PositionImpl expected");
        }
      }
      return (Position)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
