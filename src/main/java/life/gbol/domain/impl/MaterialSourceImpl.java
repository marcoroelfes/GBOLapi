package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.CollectionSampleType;
import life.gbol.domain.MaterialSource;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class MaterialSourceImpl extends OWLThingImpl implements MaterialSource {
  public static final String TypeIRI = "http://gbol.life/0.1/MaterialSource";

  protected MaterialSourceImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static MaterialSource make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new MaterialSourceImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,MaterialSource.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,MaterialSource.class,false);
          if(toRet == null) {
            toRet = new MaterialSourceImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof MaterialSource)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.MaterialSourceImpl expected");
        }
      }
      return (MaterialSource)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/collectionSampleType");
    this.checkCardMin1("http://gbol.life/0.1/collectionCode");
    this.checkCardMin1("http://gbol.life/0.1/institutionCode");
  }

  public String getSampleId() {
    return this.getStringLit("http://gbol.life/0.1/sampleId",true);
  }

  public void setSampleId(String val) {
    this.setStringLit("http://gbol.life/0.1/sampleId",val);
  }

  public CollectionSampleType getCollectionSampleType() {
    return this.getEnum("http://gbol.life/0.1/collectionSampleType",false,CollectionSampleType.class);
  }

  public void setCollectionSampleType(CollectionSampleType val) {
    this.setEnum("http://gbol.life/0.1/collectionSampleType",val,CollectionSampleType.class);
  }

  public String getCollectionCode() {
    return this.getStringLit("http://gbol.life/0.1/collectionCode",false);
  }

  public void setCollectionCode(String val) {
    this.setStringLit("http://gbol.life/0.1/collectionCode",val);
  }

  public String getInstitutionCode() {
    return this.getStringLit("http://gbol.life/0.1/institutionCode",false);
  }

  public void setInstitutionCode(String val) {
    this.setStringLit("http://gbol.life/0.1/institutionCode",val);
  }
}
