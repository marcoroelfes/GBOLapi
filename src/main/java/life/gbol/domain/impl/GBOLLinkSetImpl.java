package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Database;
import life.gbol.domain.GBOLLinkSet;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Activity;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Entity;
import org.w3.ns.prov.domain.impl.EntityImpl;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class GBOLLinkSetImpl extends EntityImpl implements GBOLLinkSet {
  public static final String TypeIRI = "http://gbol.life/0.1/GBOLLinkSet";

  protected GBOLLinkSetImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static GBOLLinkSet make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new GBOLLinkSetImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,GBOLLinkSet.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,GBOLLinkSet.class,false);
          if(toRet == null) {
            toRet = new GBOLLinkSetImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof GBOLLinkSet)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.GBOLLinkSetImpl expected");
        }
      }
      return (GBOLLinkSet)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/target");
  }

  public void remTarget(Database val) {
    this.remRef("http://gbol.life/0.1/target",val,false);
  }

  public List<? extends Database> getAllTarget() {
    return this.getRefSet("http://gbol.life/0.1/target",false,Database.class);
  }

  public void addTarget(Database val) {
    this.addRef("http://gbol.life/0.1/target",val);
  }

  public Activity getWasGeneratedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasGeneratedBy",true,Activity.class);
  }

  public void setWasGeneratedBy(Activity val) {
    this.setRef("http://www.w3.org/ns/prov#wasGeneratedBy",val,Activity.class);
  }

  public Agent getWasAttributedTo() {
    return this.getRef("http://www.w3.org/ns/prov#wasAttributedTo",true,Agent.class);
  }

  public void setWasAttributedTo(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#wasAttributedTo",val,Agent.class);
  }

  public void remWasDerivedFrom(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#wasDerivedFrom",val,true);
  }

  public List<? extends Entity> getAllWasDerivedFrom() {
    return this.getRefSet("http://www.w3.org/ns/prov#wasDerivedFrom",true,Entity.class);
  }

  public void addWasDerivedFrom(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#wasDerivedFrom",val);
  }
}
