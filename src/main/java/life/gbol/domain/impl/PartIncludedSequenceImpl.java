package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PartIncludedSequence;
import life.gbol.domain.Region;
import life.gbol.domain.UncompleteNASequence;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class PartIncludedSequenceImpl extends PartSequenceImpl implements PartIncludedSequence {
  public static final String TypeIRI = "http://gbol.life/0.1/PartIncludedSequence";

  protected PartIncludedSequenceImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static PartIncludedSequence make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PartIncludedSequenceImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,PartIncludedSequence.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,PartIncludedSequence.class,false);
          if(toRet == null) {
            toRet = new PartIncludedSequenceImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof PartIncludedSequence)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.PartIncludedSequenceImpl expected");
        }
      }
      return (PartIncludedSequence)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/includedSequence");
  }

  public UncompleteNASequence getIncludedSequence() {
    return this.getRef("http://gbol.life/0.1/includedSequence",false,UncompleteNASequence.class);
  }

  public void setIncludedSequence(UncompleteNASequence val) {
    this.setRef("http://gbol.life/0.1/includedSequence",val,UncompleteNASequence.class);
  }

  public Region getIncludedRegion() {
    return this.getRef("http://gbol.life/0.1/includedRegion",false,Region.class);
  }

  public void setIncludedRegion(Region val) {
    this.setRef("http://gbol.life/0.1/includedRegion",val,Region.class);
  }
}
