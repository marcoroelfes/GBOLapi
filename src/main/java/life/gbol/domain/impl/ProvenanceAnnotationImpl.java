package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Feature;
import life.gbol.domain.ProvenanceAnnotation;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ProvenanceAnnotationImpl extends OWLThingImpl implements ProvenanceAnnotation {
  public static final String TypeIRI = "http://gbol.life/0.1/ProvenanceAnnotation";

  protected ProvenanceAnnotationImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ProvenanceAnnotation make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ProvenanceAnnotationImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ProvenanceAnnotation.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ProvenanceAnnotation.class,false);
          if(toRet == null) {
            toRet = new ProvenanceAnnotationImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ProvenanceAnnotation)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ProvenanceAnnotationImpl expected");
        }
      }
      return (ProvenanceAnnotation)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public void remReference(Document val) {
    this.remRef("http://gbol.life/0.1/reference",val,true);
  }

  public List<? extends Document> getAllReference() {
    return this.getRefSet("http://gbol.life/0.1/reference",true,Document.class);
  }

  public void addReference(Document val) {
    this.addRef("http://gbol.life/0.1/reference",val);
  }

  public void remDerivedFrom(Feature val) {
    this.remRef("http://gbol.life/0.1/derivedFrom",val,true);
  }

  public List<? extends Feature> getAllDerivedFrom() {
    return this.getRefSet("http://gbol.life/0.1/derivedFrom",true,Feature.class);
  }

  public void addDerivedFrom(Feature val) {
    this.addRef("http://gbol.life/0.1/derivedFrom",val);
  }

  public String getProvenanceNote() {
    return this.getStringLit("http://gbol.life/0.1/provenanceNote",true);
  }

  public void setProvenanceNote(String val) {
    this.setStringLit("http://gbol.life/0.1/provenanceNote",val);
  }

  public String getExperiment() {
    return this.getStringLit("http://gbol.life/0.1/experiment",true);
  }

  public void setExperiment(String val) {
    this.setStringLit("http://gbol.life/0.1/experiment",val);
  }
}
