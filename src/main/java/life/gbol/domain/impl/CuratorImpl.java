package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Curator;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.impl.PersonImpl;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class CuratorImpl extends PersonImpl implements Curator {
  public static final String TypeIRI = "http://gbol.life/0.1/Curator";

  protected CuratorImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Curator make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new CuratorImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Curator.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Curator.class,false);
          if(toRet == null) {
            toRet = new CuratorImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Curator)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.CuratorImpl expected");
        }
      }
      return (Curator)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/orcid");
  }

  public String getOrcid() {
    return this.getExternalRef("http://gbol.life/0.1/orcid",false);
  }

  public void setOrcid(String val) {
    this.setExternalRef("http://gbol.life/0.1/orcid",val);
  }

  public Agent getActedOnBehalfOf() {
    return this.getRef("http://www.w3.org/ns/prov#actedOnBehalfOf",true,Agent.class);
  }

  public void setActedOnBehalfOf(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#actedOnBehalfOf",val,Agent.class);
  }

  public String getName() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/name",false);
  }

  public void setName(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/name",val);
  }

  public String getPhone() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/phone",true);
  }

  public void setPhone(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/phone",val);
  }

  public String getSuffixName() {
    return this.getStringLit("http://purl.org/ontology/bibo/suffixName",true);
  }

  public void setSuffixName(String val) {
    this.setStringLit("http://purl.org/ontology/bibo/suffixName",val);
  }

  public String getGivenName() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/givenName",true);
  }

  public void setGivenName(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/givenName",val);
  }

  public String getSurName() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/surName",true);
  }

  public void setSurName(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/surName",val);
  }

  public String getPrefixName() {
    return this.getStringLit("http://purl.org/ontology/bibo/prefixName",true);
  }

  public void setPrefixName(String val) {
    this.setStringLit("http://purl.org/ontology/bibo/prefixName",val);
  }

  public String getDepiction() {
    return this.getExternalRef("http://xmlns.com/foaf/0.1/depiction",true);
  }

  public void setDepiction(String val) {
    this.setExternalRef("http://xmlns.com/foaf/0.1/depiction",val);
  }

  public String getFamilyName() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/familyName",true);
  }

  public void setFamilyName(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/familyName",val);
  }

  public String getLocalityName() {
    return this.getStringLit("http://purl.org/ontology/bibo/localityName",true);
  }

  public void setLocalityName(String val) {
    this.setStringLit("http://purl.org/ontology/bibo/localityName",val);
  }

  public String getTitle() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/title",true);
  }

  public void setTitle(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/title",val);
  }

  public String getFirstName() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/firstName",true);
  }

  public void setFirstName(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/firstName",val);
  }

  public String getHomepage() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/homepage",true);
  }

  public void setHomepage(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/homepage",val);
  }

  public String getMbox() {
    return this.getExternalRef("http://xmlns.com/foaf/0.1/mbox",true);
  }

  public void setMbox(String val) {
    this.setExternalRef("http://xmlns.com/foaf/0.1/mbox",val);
  }
}
