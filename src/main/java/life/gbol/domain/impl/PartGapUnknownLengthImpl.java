package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PartGapUnknownLength;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class PartGapUnknownLengthImpl extends PartGapImpl implements PartGapUnknownLength {
  public static final String TypeIRI = "http://gbol.life/0.1/PartGapUnknownLength";

  protected PartGapUnknownLengthImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static PartGapUnknownLength make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PartGapUnknownLengthImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,PartGapUnknownLength.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,PartGapUnknownLength.class,false);
          if(toRet == null) {
            toRet = new PartGapUnknownLengthImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof PartGapUnknownLength)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.PartGapUnknownLengthImpl expected");
        }
      }
      return (PartGapUnknownLength)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
