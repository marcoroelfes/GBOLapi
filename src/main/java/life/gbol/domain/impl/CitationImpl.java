package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Citation;
import life.gbol.domain.Provenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class CitationImpl extends QualifierImpl implements Citation {
  public static final String TypeIRI = "http://gbol.life/0.1/Citation";

  protected CitationImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Citation make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new CitationImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Citation.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Citation.class,false);
          if(toRet == null) {
            toRet = new CitationImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Citation)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.CitationImpl expected");
        }
      }
      return (Citation)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/reference");
  }

  public Document getReference() {
    return this.getRef("http://gbol.life/0.1/reference",false,Document.class);
  }

  public void setReference(Document val) {
    this.setRef("http://gbol.life/0.1/reference",val,Document.class);
  }

  public String getHighlight() {
    return this.getStringLit("http://gbol.life/0.1/highlight",true);
  }

  public void setHighlight(String val) {
    this.setStringLit("http://gbol.life/0.1/highlight",val);
  }

  public Provenance getProvenance() {
    return this.getRef("http://gbol.life/0.1/provenance",false,Provenance.class);
  }

  public void setProvenance(Provenance val) {
    this.setRef("http://gbol.life/0.1/provenance",val,Provenance.class);
  }
}
