package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AminoAcid;
import life.gbol.domain.Region;
import life.gbol.domain.TranslExcept;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class TranslExceptImpl extends OWLThingImpl implements TranslExcept {
  public static final String TypeIRI = "http://gbol.life/0.1/TranslExcept";

  protected TranslExceptImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static TranslExcept make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new TranslExceptImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,TranslExcept.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,TranslExcept.class,false);
          if(toRet == null) {
            toRet = new TranslExceptImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof TranslExcept)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.TranslExceptImpl expected");
        }
      }
      return (TranslExcept)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/exceptRegion");
    this.checkCardMin1("http://gbol.life/0.1/aminoAcid");
  }

  public Region getExceptRegion() {
    return this.getRef("http://gbol.life/0.1/exceptRegion",false,Region.class);
  }

  public void setExceptRegion(Region val) {
    this.setRef("http://gbol.life/0.1/exceptRegion",val,Region.class);
  }

  public AminoAcid getAminoAcid() {
    return this.getEnum("http://gbol.life/0.1/aminoAcid",false,AminoAcid.class);
  }

  public void setAminoAcid(AminoAcid val) {
    this.setEnum("http://gbol.life/0.1/aminoAcid",val,AminoAcid.class);
  }
}
