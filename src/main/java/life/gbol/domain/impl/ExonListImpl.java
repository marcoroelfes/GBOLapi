package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Exon;
import life.gbol.domain.ExonList;
import life.gbol.domain.Provenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ExonListImpl extends OWLThingImpl implements ExonList {
  public static final String TypeIRI = "http://gbol.life/0.1/ExonList";

  protected ExonListImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ExonList make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ExonListImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ExonList.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ExonList.class,false);
          if(toRet == null) {
            toRet = new ExonListImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ExonList)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ExonListImpl expected");
        }
      }
      return (ExonList)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/exon");
  }

  public Exon getExon(int index) {
    return this.getRefListAtIndex("http://gbol.life/0.1/exon",false,Exon.class,index);
  }

  public List<? extends Exon> getAllExon() {
    return this.getRefList("http://gbol.life/0.1/exon",false,Exon.class);
  }

  public void addExon(Exon val) {
    this.addRefList("http://gbol.life/0.1/exon",val);
  }

  public void setExon(Exon val, int index) {
    this.setRefList("http://gbol.life/0.1/exon",val,false,index);
  }

  public void remExon(Exon val) {
    this.remRefList("http://gbol.life/0.1/exon",val,false);
  }

  public String getPhenotype() {
    return this.getStringLit("http://gbol.life/0.1/phenotype",true);
  }

  public void setPhenotype(String val) {
    this.setStringLit("http://gbol.life/0.1/phenotype",val);
  }

  public Provenance getProvenance() {
    return this.getRef("http://gbol.life/0.1/provenance",true,Provenance.class);
  }

  public void setProvenance(Provenance val) {
    this.setRef("http://gbol.life/0.1/provenance",val,Provenance.class);
  }
}
