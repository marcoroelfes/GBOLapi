package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PCRPrimer;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class PCRPrimerImpl extends OWLThingImpl implements PCRPrimer {
  public static final String TypeIRI = "http://gbol.life/0.1/PCRPrimer";

  protected PCRPrimerImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static PCRPrimer make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PCRPrimerImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,PCRPrimer.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,PCRPrimer.class,false);
          if(toRet == null) {
            toRet = new PCRPrimerImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof PCRPrimer)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.PCRPrimerImpl expected");
        }
      }
      return (PCRPrimer)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/primerSequence");
  }

  public String getPrimerName() {
    return this.getStringLit("http://gbol.life/0.1/primerName",true);
  }

  public void setPrimerName(String val) {
    this.setStringLit("http://gbol.life/0.1/primerName",val);
  }

  public String getPrimerSequence() {
    return this.getStringLit("http://gbol.life/0.1/primerSequence",false);
  }

  public void setPrimerSequence(String val) {
    this.setStringLit("http://gbol.life/0.1/primerSequence",val);
  }
}
