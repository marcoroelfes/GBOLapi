package life.gbol.domain.impl;

import java.lang.Long;
import java.lang.String;
import life.gbol.domain.InRangePosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class InRangePositionImpl extends FuzzyPositionImpl implements InRangePosition {
  public static final String TypeIRI = "http://gbol.life/0.1/InRangePosition";

  protected InRangePositionImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static InRangePosition make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new InRangePositionImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,InRangePosition.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,InRangePosition.class,false);
          if(toRet == null) {
            toRet = new InRangePositionImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof InRangePosition)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.InRangePositionImpl expected");
        }
      }
      return (InRangePosition)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/endPosition");
    this.checkCardMin1("http://gbol.life/0.1/beginPosition");
  }

  public Long getEndPosition() {
    return this.getLongLit("http://gbol.life/0.1/endPosition",false);
  }

  public void setEndPosition(Long val) {
    this.setLongLit("http://gbol.life/0.1/endPosition",val);
  }

  public Long getBeginPosition() {
    return this.getLongLit("http://gbol.life/0.1/beginPosition",false);
  }

  public void setBeginPosition(Long val) {
    this.setLongLit("http://gbol.life/0.1/beginPosition",val);
  }
}
