package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Satellite;
import life.gbol.domain.SatelliteType;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class SatelliteImpl extends OWLThingImpl implements Satellite {
  public static final String TypeIRI = "http://gbol.life/0.1/Satellite";

  protected SatelliteImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Satellite make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new SatelliteImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Satellite.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Satellite.class,false);
          if(toRet == null) {
            toRet = new SatelliteImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Satellite)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.SatelliteImpl expected");
        }
      }
      return (Satellite)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/satelliteName");
    this.checkCardMin1("http://gbol.life/0.1/satelliteType");
  }

  public String getSatelliteName() {
    return this.getStringLit("http://gbol.life/0.1/satelliteName",false);
  }

  public void setSatelliteName(String val) {
    this.setStringLit("http://gbol.life/0.1/satelliteName",val);
  }

  public SatelliteType getSatelliteType() {
    return this.getEnum("http://gbol.life/0.1/satelliteType",false,SatelliteType.class);
  }

  public void setSatelliteType(SatelliteType val) {
    this.setEnum("http://gbol.life/0.1/satelliteType",val,SatelliteType.class);
  }
}
