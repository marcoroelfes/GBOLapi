package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Citation;
import life.gbol.domain.FeatureProvenance;
import life.gbol.domain.Location;
import life.gbol.domain.Note;
import life.gbol.domain.ProteinRepeat;
import life.gbol.domain.ReasonArtificialLocation;
import life.gbol.domain.Region;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ProteinRepeatImpl extends ProteinFeatureImpl implements ProteinRepeat {
  public static final String TypeIRI = "http://gbol.life/0.1/ProteinRepeat";

  protected ProteinRepeatImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ProteinRepeat make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ProteinRepeatImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ProteinRepeat.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ProteinRepeat.class,false);
          if(toRet == null) {
            toRet = new ProteinRepeatImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ProteinRepeat)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ProteinRepeatImpl expected");
        }
      }
      return (ProteinRepeat)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public String getRptUnitSeq() {
    return this.getStringLit("http://gbol.life/0.1/rptUnitSeq",true);
  }

  public void setRptUnitSeq(String val) {
    this.setStringLit("http://gbol.life/0.1/rptUnitSeq",val);
  }

  public Region getRptUnitRange() {
    return this.getRef("http://gbol.life/0.1/rptUnitRange",true,Region.class);
  }

  public void setRptUnitRange(Region val) {
    this.setRef("http://gbol.life/0.1/rptUnitRange",val,Region.class);
  }

  public Location getLocation() {
    return this.getRef("http://gbol.life/0.1/location",false,Location.class);
  }

  public void setLocation(Location val) {
    this.setRef("http://gbol.life/0.1/location",val,Location.class);
  }

  public void remCitation(Citation val) {
    this.remRef("http://gbol.life/0.1/citation",val,true);
  }

  public List<? extends Citation> getAllCitation() {
    return this.getRefSet("http://gbol.life/0.1/citation",true,Citation.class);
  }

  public void addCitation(Citation val) {
    this.addRef("http://gbol.life/0.1/citation",val);
  }

  public String getFunction() {
    return this.getStringLit("http://gbol.life/0.1/function",true);
  }

  public void setFunction(String val) {
    this.setStringLit("http://gbol.life/0.1/function",val);
  }

  public String getPhenotype() {
    return this.getStringLit("http://gbol.life/0.1/phenotype",true);
  }

  public void setPhenotype(String val) {
    this.setStringLit("http://gbol.life/0.1/phenotype",val);
  }

  public void remProvenance(FeatureProvenance val) {
    this.remRef("http://gbol.life/0.1/provenance",val,false);
  }

  public List<? extends FeatureProvenance> getAllProvenance() {
    return this.getRefSet("http://gbol.life/0.1/provenance",false,FeatureProvenance.class);
  }

  public void addProvenance(FeatureProvenance val) {
    this.addRef("http://gbol.life/0.1/provenance",val);
  }

  public ReasonArtificialLocation getArtificialLocation() {
    return this.getEnum("http://gbol.life/0.1/artificialLocation",true,ReasonArtificialLocation.class);
  }

  public void setArtificialLocation(ReasonArtificialLocation val) {
    this.setEnum("http://gbol.life/0.1/artificialLocation",val,ReasonArtificialLocation.class);
  }

  public void remAccession(String val) {
    this.remStringLit("http://gbol.life/0.1/accession",val,true);
  }

  public List<? extends String> getAllAccession() {
    return this.getStringLitSet("http://gbol.life/0.1/accession",true);
  }

  public void addAccession(String val) {
    this.addStringLit("http://gbol.life/0.1/accession",val);
  }

  public String getStandardName() {
    return this.getStringLit("http://gbol.life/0.1/standardName",true);
  }

  public void setStandardName(String val) {
    this.setStringLit("http://gbol.life/0.1/standardName",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life/0.1/xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life/0.1/xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life/0.1/xref",val);
  }

  public void remNote(Note val) {
    this.remRef("http://gbol.life/0.1/note",val,true);
  }

  public List<? extends Note> getAllNote() {
    return this.getRefSet("http://gbol.life/0.1/note",true,Note.class);
  }

  public void addNote(Note val) {
    this.addRef("http://gbol.life/0.1/note",val);
  }
}
