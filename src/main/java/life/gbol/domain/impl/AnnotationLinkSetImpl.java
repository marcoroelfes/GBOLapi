package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.AnnotationActivity;
import life.gbol.domain.AnnotationLinkSet;
import life.gbol.domain.Database;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class AnnotationLinkSetImpl extends AnnotationResultImpl implements AnnotationLinkSet {
  public static final String TypeIRI = "http://gbol.life/0.1/AnnotationLinkSet";

  protected AnnotationLinkSetImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static AnnotationLinkSet make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new AnnotationLinkSetImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,AnnotationLinkSet.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,AnnotationLinkSet.class,false);
          if(toRet == null) {
            toRet = new AnnotationLinkSetImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof AnnotationLinkSet)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.AnnotationLinkSetImpl expected");
        }
      }
      return (AnnotationLinkSet)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public AnnotationActivity getWasGeneratedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasGeneratedBy",false,AnnotationActivity.class);
  }

  public void setWasGeneratedBy(AnnotationActivity val) {
    this.setRef("http://www.w3.org/ns/prov#wasGeneratedBy",val,AnnotationActivity.class);
  }

  public Agent getWasAttributedTo() {
    return this.getRef("http://www.w3.org/ns/prov#wasAttributedTo",false,Agent.class);
  }

  public void setWasAttributedTo(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#wasAttributedTo",val,Agent.class);
  }

  public void remWasDerivedFrom(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#wasDerivedFrom",val,true);
  }

  public List<? extends Entity> getAllWasDerivedFrom() {
    return this.getRefSet("http://www.w3.org/ns/prov#wasDerivedFrom",true,Entity.class);
  }

  public void addWasDerivedFrom(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#wasDerivedFrom",val);
  }

  public void remTarget(Database val) {
    this.remRef("http://gbol.life/0.1/target",val,false);
  }

  public List<? extends Database> getAllTarget() {
    return this.getRefSet("http://gbol.life/0.1/target",false,Database.class);
  }

  public void addTarget(Database val) {
    this.addRef("http://gbol.life/0.1/target",val);
  }
}
