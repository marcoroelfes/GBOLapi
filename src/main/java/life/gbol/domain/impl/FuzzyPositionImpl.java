package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.FuzzyPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class FuzzyPositionImpl extends PositionImpl implements FuzzyPosition {
  public static final String TypeIRI = "http://gbol.life/0.1/FuzzyPosition";

  protected FuzzyPositionImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static FuzzyPosition make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new FuzzyPositionImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,FuzzyPosition.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,FuzzyPosition.class,false);
          if(toRet == null) {
            toRet = new FuzzyPositionImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof FuzzyPosition)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.FuzzyPositionImpl expected");
        }
      }
      return (FuzzyPosition)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
