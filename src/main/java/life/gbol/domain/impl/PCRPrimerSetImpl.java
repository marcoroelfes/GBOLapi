package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.PCRPrimer;
import life.gbol.domain.PCRPrimerSet;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class PCRPrimerSetImpl extends OWLThingImpl implements PCRPrimerSet {
  public static final String TypeIRI = "http://gbol.life/0.1/PCRPrimerSet";

  protected PCRPrimerSetImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static PCRPrimerSet make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PCRPrimerSetImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,PCRPrimerSet.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,PCRPrimerSet.class,false);
          if(toRet == null) {
            toRet = new PCRPrimerSetImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof PCRPrimerSet)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.PCRPrimerSetImpl expected");
        }
      }
      return (PCRPrimerSet)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/reversePrimer");
    this.checkCardMin1("http://gbol.life/0.1/forwardPrimer");
  }

  public void remReversePrimer(PCRPrimer val) {
    this.remRef("http://gbol.life/0.1/reversePrimer",val,false);
  }

  public List<? extends PCRPrimer> getAllReversePrimer() {
    return this.getRefSet("http://gbol.life/0.1/reversePrimer",false,PCRPrimer.class);
  }

  public void addReversePrimer(PCRPrimer val) {
    this.addRef("http://gbol.life/0.1/reversePrimer",val);
  }

  public void remForwardPrimer(PCRPrimer val) {
    this.remRef("http://gbol.life/0.1/forwardPrimer",val,false);
  }

  public List<? extends PCRPrimer> getAllForwardPrimer() {
    return this.getRefSet("http://gbol.life/0.1/forwardPrimer",false,PCRPrimer.class);
  }

  public void addForwardPrimer(PCRPrimer val) {
    this.addRef("http://gbol.life/0.1/forwardPrimer",val);
  }
}
