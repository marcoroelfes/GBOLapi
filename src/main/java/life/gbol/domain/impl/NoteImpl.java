package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Note;
import life.gbol.domain.Provenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class NoteImpl extends QualifierImpl implements Note {
  public static final String TypeIRI = "http://gbol.life/0.1/Note";

  protected NoteImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Note make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new NoteImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Note.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Note.class,false);
          if(toRet == null) {
            toRet = new NoteImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Note)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.NoteImpl expected");
        }
      }
      return (Note)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/provenance");
    this.checkCardMin1("http://gbol.life/0.1/text");
  }

  public Provenance getProvenance() {
    return this.getRef("http://gbol.life/0.1/provenance",false,Provenance.class);
  }

  public void setProvenance(Provenance val) {
    this.setRef("http://gbol.life/0.1/provenance",val,Provenance.class);
  }

  public String getText() {
    return this.getStringLit("http://gbol.life/0.1/text",false);
  }

  public void setText(String val) {
    this.setStringLit("http://gbol.life/0.1/text",val);
  }
}
