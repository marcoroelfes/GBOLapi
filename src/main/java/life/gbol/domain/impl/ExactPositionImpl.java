package life.gbol.domain.impl;

import java.lang.Long;
import java.lang.String;
import life.gbol.domain.ExactPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ExactPositionImpl extends PositionImpl implements ExactPosition {
  public static final String TypeIRI = "http://gbol.life/0.1/ExactPosition";

  protected ExactPositionImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ExactPosition make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ExactPositionImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ExactPosition.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ExactPosition.class,false);
          if(toRet == null) {
            toRet = new ExactPositionImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ExactPosition)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ExactPositionImpl expected");
        }
      }
      return (ExactPosition)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/position");
  }

  public Long getPosition() {
    return this.getLongLit("http://gbol.life/0.1/position",false);
  }

  public void setPosition(Long val) {
    this.setLongLit("http://gbol.life/0.1/position",val);
  }
}
