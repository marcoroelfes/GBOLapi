package life.gbol.domain.impl;

import java.lang.Long;
import java.lang.String;
import life.gbol.domain.AfterPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class AfterPositionImpl extends FuzzyPositionImpl implements AfterPosition {
  public static final String TypeIRI = "http://gbol.life/0.1/AfterPosition";

  protected AfterPositionImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static AfterPosition make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new AfterPositionImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,AfterPosition.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,AfterPosition.class,false);
          if(toRet == null) {
            toRet = new AfterPositionImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof AfterPosition)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.AfterPositionImpl expected");
        }
      }
      return (AfterPosition)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/position");
  }

  public Long getPosition() {
    return this.getLongLit("http://gbol.life/0.1/position",false);
  }

  public void setPosition(Long val) {
    this.setLongLit("http://gbol.life/0.1/position",val);
  }
}
