package life.gbol.domain.impl;

import java.lang.Long;
import java.lang.String;
import life.gbol.domain.BeforePosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class BeforePositionImpl extends FuzzyPositionImpl implements BeforePosition {
  public static final String TypeIRI = "http://gbol.life/0.1/BeforePosition";

  protected BeforePositionImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static BeforePosition make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new BeforePositionImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,BeforePosition.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,BeforePosition.class,false);
          if(toRet == null) {
            toRet = new BeforePositionImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof BeforePosition)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.BeforePositionImpl expected");
        }
      }
      return (BeforePosition)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/position");
  }

  public Long getPosition() {
    return this.getLongLit("http://gbol.life/0.1/position",false);
  }

  public void setPosition(Long val) {
    this.setLongLit("http://gbol.life/0.1/position",val);
  }
}
