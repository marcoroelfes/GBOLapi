package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PartGap;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class PartGapImpl extends SequencePartImpl implements PartGap {
  public static final String TypeIRI = "http://gbol.life/0.1/PartGap";

  protected PartGapImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static PartGap make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PartGapImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,PartGap.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,PartGap.class,false);
          if(toRet == null) {
            toRet = new PartGapImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof PartGap)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.PartGapImpl expected");
        }
      }
      return (PartGap)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
