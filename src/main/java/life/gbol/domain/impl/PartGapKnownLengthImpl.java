package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.PartGapKnownLength;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class PartGapKnownLengthImpl extends PartGapImpl implements PartGapKnownLength {
  public static final String TypeIRI = "http://gbol.life/0.1/PartGapKnownLength";

  protected PartGapKnownLengthImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static PartGapKnownLength make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PartGapKnownLengthImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,PartGapKnownLength.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,PartGapKnownLength.class,false);
          if(toRet == null) {
            toRet = new PartGapKnownLengthImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof PartGapKnownLength)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.PartGapKnownLengthImpl expected");
        }
      }
      return (PartGapKnownLength)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/gapLength");
  }

  public Integer getGapLength() {
    return this.getIntegerLit("http://gbol.life/0.1/gapLength",false);
  }

  public void setGapLength(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/gapLength",val);
  }
}
