package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.SequencePart;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class SequencePartImpl extends OWLThingImpl implements SequencePart {
  public static final String TypeIRI = "http://gbol.life/0.1/SequencePart";

  protected SequencePartImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static SequencePart make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new SequencePartImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,SequencePart.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,SequencePart.class,false);
          if(toRet == null) {
            toRet = new SequencePartImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof SequencePart)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.SequencePartImpl expected");
        }
      }
      return (SequencePart)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
