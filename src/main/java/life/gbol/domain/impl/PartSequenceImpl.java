package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PartSequence;
import life.gbol.domain.Region;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class PartSequenceImpl extends SequencePartImpl implements PartSequence {
  public static final String TypeIRI = "http://gbol.life/0.1/PartSequence";

  protected PartSequenceImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static PartSequence make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PartSequenceImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,PartSequence.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,PartSequence.class,false);
          if(toRet == null) {
            toRet = new PartSequenceImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof PartSequence)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.PartSequenceImpl expected");
        }
      }
      return (PartSequence)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/includedRegion");
  }

  public Region getIncludedRegion() {
    return this.getRef("http://gbol.life/0.1/includedRegion",false,Region.class);
  }

  public void setIncludedRegion(Region val) {
    this.setRef("http://gbol.life/0.1/includedRegion",val,Region.class);
  }
}
