package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Provenance;
import life.gbol.domain.SequenceAssembly;
import life.gbol.domain.SequencePart;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class SequenceAssemblyImpl extends OWLThingImpl implements SequenceAssembly {
  public static final String TypeIRI = "http://gbol.life/0.1/SequenceAssembly";

  protected SequenceAssemblyImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static SequenceAssembly make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new SequenceAssemblyImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,SequenceAssembly.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,SequenceAssembly.class,false);
          if(toRet == null) {
            toRet = new SequenceAssemblyImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof SequenceAssembly)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.SequenceAssemblyImpl expected");
        }
      }
      return (SequenceAssembly)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/sequencePart");
  }

  public Provenance getProvenance() {
    return this.getRef("http://gbol.life/0.1/provenance",true,Provenance.class);
  }

  public void setProvenance(Provenance val) {
    this.setRef("http://gbol.life/0.1/provenance",val,Provenance.class);
  }

  public SequencePart getSequencePart(int index) {
    return this.getRefListAtIndex("http://gbol.life/0.1/sequencePart",false,SequencePart.class,index);
  }

  public List<? extends SequencePart> getAllSequencePart() {
    return this.getRefList("http://gbol.life/0.1/sequencePart",false,SequencePart.class);
  }

  public void addSequencePart(SequencePart val) {
    this.addRefList("http://gbol.life/0.1/sequencePart",val);
  }

  public void setSequencePart(SequencePart val, int index) {
    this.setRefList("http://gbol.life/0.1/sequencePart",val,false,index);
  }

  public void remSequencePart(SequencePart val) {
    this.remRefList("http://gbol.life/0.1/sequencePart",val,false);
  }
}
