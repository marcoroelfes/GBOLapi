package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.AnnotationActivity;
import life.gbol.domain.AnnotationResult;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Entity;
import org.w3.ns.prov.domain.impl.EntityImpl;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class AnnotationResultImpl extends EntityImpl implements AnnotationResult {
  public static final String TypeIRI = "http://gbol.life/0.1/AnnotationResult";

  protected AnnotationResultImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static AnnotationResult make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new AnnotationResultImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,AnnotationResult.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,AnnotationResult.class,false);
          if(toRet == null) {
            toRet = new AnnotationResultImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof AnnotationResult)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.AnnotationResultImpl expected");
        }
      }
      return (AnnotationResult)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://www.w3.org/ns/prov#wasGeneratedBy");
    this.checkCardMin1("http://www.w3.org/ns/prov#wasAttributedTo");
  }

  public AnnotationActivity getWasGeneratedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasGeneratedBy",false,AnnotationActivity.class);
  }

  public void setWasGeneratedBy(AnnotationActivity val) {
    this.setRef("http://www.w3.org/ns/prov#wasGeneratedBy",val,AnnotationActivity.class);
  }

  public Agent getWasAttributedTo() {
    return this.getRef("http://www.w3.org/ns/prov#wasAttributedTo",false,Agent.class);
  }

  public void setWasAttributedTo(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#wasAttributedTo",val,Agent.class);
  }

  public void remWasDerivedFrom(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#wasDerivedFrom",val,true);
  }

  public List<? extends Entity> getAllWasDerivedFrom() {
    return this.getRefSet("http://www.w3.org/ns/prov#wasDerivedFrom",true,Entity.class);
  }

  public void addWasDerivedFrom(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#wasDerivedFrom",val);
  }
}
