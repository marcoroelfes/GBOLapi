package life.gbol.domain.impl;

import java.lang.String;
import java.time.LocalDateTime;
import java.util.List;
import life.gbol.domain.AnnotationSoftware;
import life.gbol.domain.AutomaticAnnotationActivity;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Activity;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class AutomaticAnnotationActivityImpl extends AnnotationActivityImpl implements AutomaticAnnotationActivity {
  public static final String TypeIRI = "http://gbol.life/0.1/AutomaticAnnotationActivity";

  protected AutomaticAnnotationActivityImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static AutomaticAnnotationActivity make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new AutomaticAnnotationActivityImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,AutomaticAnnotationActivity.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,AutomaticAnnotationActivity.class,false);
          if(toRet == null) {
            toRet = new AutomaticAnnotationActivityImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof AutomaticAnnotationActivity)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.AutomaticAnnotationActivityImpl expected");
        }
      }
      return (AutomaticAnnotationActivity)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/parameterString");
  }

  public AnnotationSoftware getWasAssociatedWith() {
    return this.getRef("http://gbol.life/0.1/wasAssociatedWith",true,AnnotationSoftware.class);
  }

  public void setWasAssociatedWith(AnnotationSoftware val) {
    this.setRef("http://gbol.life/0.1/wasAssociatedWith",val,AnnotationSoftware.class);
  }

  public String getParameterString() {
    return this.getStringLit("http://gbol.life/0.1/parameterString",false);
  }

  public void setParameterString(String val) {
    this.setStringLit("http://gbol.life/0.1/parameterString",val);
  }

  public LocalDateTime getStartedAtTime() {
    return this.getDateTimeLit("http://www.w3.org/ns/prov#startedAtTime",false);
  }

  public void setStartedAtTime(LocalDateTime val) {
    this.setDateTimeLit("http://www.w3.org/ns/prov#startedAtTime",val);
  }

  public LocalDateTime getEndedAtTime() {
    return this.getDateTimeLit("http://www.w3.org/ns/prov#endedAtTime",false);
  }

  public void setEndedAtTime(LocalDateTime val) {
    this.setDateTimeLit("http://www.w3.org/ns/prov#endedAtTime",val);
  }

  public void remUsed(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#used",val,true);
  }

  public List<? extends Entity> getAllUsed() {
    return this.getRefSet("http://www.w3.org/ns/prov#used",true,Entity.class);
  }

  public void addUsed(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#used",val);
  }

  public void remGenerated(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#generated",val,true);
  }

  public List<? extends Entity> getAllGenerated() {
    return this.getRefSet("http://www.w3.org/ns/prov#generated",true,Entity.class);
  }

  public void addGenerated(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#generated",val);
  }

  public Activity getWasInformedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasInformedBy",true,Activity.class);
  }

  public void setWasInformedBy(Activity val) {
    this.setRef("http://www.w3.org/ns/prov#wasInformedBy",val,Activity.class);
  }
}
