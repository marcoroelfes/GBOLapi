package life.gbol.domain.impl;

import java.lang.Long;
import java.lang.String;
import java.util.List;
import life.gbol.domain.OneOfPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class OneOfPositionImpl extends FuzzyPositionImpl implements OneOfPosition {
  public static final String TypeIRI = "http://gbol.life/0.1/OneOfPosition";

  protected OneOfPositionImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static OneOfPosition make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new OneOfPositionImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,OneOfPosition.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,OneOfPosition.class,false);
          if(toRet == null) {
            toRet = new OneOfPositionImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof OneOfPosition)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.OneOfPositionImpl expected");
        }
      }
      return (OneOfPosition)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/position");
  }

  public void remPosition(Long val) {
    this.remLongLit("http://gbol.life/0.1/position",val,false);
  }

  public List<? extends Long> getAllPosition() {
    return this.getLongLitSet("http://gbol.life/0.1/position",false);
  }

  public void addPosition(Long val) {
    this.addLongLit("http://gbol.life/0.1/position",val);
  }
}
