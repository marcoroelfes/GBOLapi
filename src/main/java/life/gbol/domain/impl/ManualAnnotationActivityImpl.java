package life.gbol.domain.impl;

import java.lang.String;
import java.time.LocalDateTime;
import java.util.List;
import life.gbol.domain.Curator;
import life.gbol.domain.ManualAnnotationActivity;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Activity;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ManualAnnotationActivityImpl extends AnnotationActivityImpl implements ManualAnnotationActivity {
  public static final String TypeIRI = "http://gbol.life/0.1/ManualAnnotationActivity";

  protected ManualAnnotationActivityImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ManualAnnotationActivity make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ManualAnnotationActivityImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ManualAnnotationActivity.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ManualAnnotationActivity.class,false);
          if(toRet == null) {
            toRet = new ManualAnnotationActivityImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ManualAnnotationActivity)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ManualAnnotationActivityImpl expected");
        }
      }
      return (ManualAnnotationActivity)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public Curator getWasAssociatedWith() {
    return this.getRef("http://gbol.life/0.1/wasAssociatedWith",true,Curator.class);
  }

  public void setWasAssociatedWith(Curator val) {
    this.setRef("http://gbol.life/0.1/wasAssociatedWith",val,Curator.class);
  }

  public LocalDateTime getStartedAtTime() {
    return this.getDateTimeLit("http://www.w3.org/ns/prov#startedAtTime",false);
  }

  public void setStartedAtTime(LocalDateTime val) {
    this.setDateTimeLit("http://www.w3.org/ns/prov#startedAtTime",val);
  }

  public LocalDateTime getEndedAtTime() {
    return this.getDateTimeLit("http://www.w3.org/ns/prov#endedAtTime",false);
  }

  public void setEndedAtTime(LocalDateTime val) {
    this.setDateTimeLit("http://www.w3.org/ns/prov#endedAtTime",val);
  }

  public void remUsed(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#used",val,true);
  }

  public List<? extends Entity> getAllUsed() {
    return this.getRefSet("http://www.w3.org/ns/prov#used",true,Entity.class);
  }

  public void addUsed(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#used",val);
  }

  public void remGenerated(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#generated",val,true);
  }

  public List<? extends Entity> getAllGenerated() {
    return this.getRefSet("http://www.w3.org/ns/prov#generated",true,Entity.class);
  }

  public void addGenerated(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#generated",val);
  }

  public Activity getWasInformedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasInformedBy",true,Activity.class);
  }

  public void setWasInformedBy(Activity val) {
    this.setRef("http://www.w3.org/ns/prov#wasInformedBy",val,Activity.class);
  }
}
