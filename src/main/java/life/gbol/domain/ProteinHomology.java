package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ProteinHomology extends ProteinFeature {
  String getHomologousToDesc();

  void setHomologousToDesc(String val);

  String getHomologousTo();

  void setHomologousTo(String val);

  Region getTargetRegion();

  void setTargetRegion(Region val);
}
