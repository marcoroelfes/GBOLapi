package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface CrossLink extends ProteinFeature {
  String getCrossLinkType();

  void setCrossLinkType(String val);

  CrossLink getCrossLinkTo();

  void setCrossLinkTo(CrossLink val);
}
