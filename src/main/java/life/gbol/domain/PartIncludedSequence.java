package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface PartIncludedSequence extends PartSequence {
  UncompleteNASequence getIncludedSequence();

  void setIncludedSequence(UncompleteNASequence val);
}
