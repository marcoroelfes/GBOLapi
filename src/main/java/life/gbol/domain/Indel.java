package life.gbol.domain;

import java.lang.Integer;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Indel extends Variation {
  Integer getAltlength();

  void setAltlength(Integer val);

  Integer getReflength();

  void setReflength(Integer val);
}
