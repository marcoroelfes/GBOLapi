package life.gbol.domain;

import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface CollectionOfRegions extends Location {
  void remMembers(Region val);

  List<? extends Region> getAllMembers();

  void addMembers(Region val);
}
