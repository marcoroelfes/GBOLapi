package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface UpdatedSequence extends VariationFeature {
  String getCompare();

  void setCompare(String val);
}
