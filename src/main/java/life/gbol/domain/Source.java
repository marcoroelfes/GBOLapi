package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Source extends NAFeature {
  Sample getSample();

  void setSample(Sample val);

  Organism getOrganism();

  void setOrganism(Organism val);
}
