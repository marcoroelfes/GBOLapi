package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface InBetween extends Location {
  Position getAfter();

  void setAfter(Position val);

  StrandPosition getStrand();

  void setStrand(StrandPosition val);
}
