package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface AntiCodon extends OWLThing {
  String getAntiCodonSequence();

  void setAntiCodonSequence(String val);

  AminoAcid getAminoAcid();

  void setAminoAcid(AminoAcid val);

  Region getAntiCodingRegion();

  void setAntiCodingRegion(Region val);
}
