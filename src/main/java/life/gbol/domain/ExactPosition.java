package life.gbol.domain;

import java.lang.Long;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ExactPosition extends Position {
  Long getPosition();

  void setPosition(Long val);
}
