package life.gbol.domain;

import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface PCRPrimerSet extends OWLThing {
  void remReversePrimer(PCRPrimer val);

  List<? extends PCRPrimer> getAllReversePrimer();

  void addReversePrimer(PCRPrimer val);

  void remForwardPrimer(PCRPrimer val);

  List<? extends PCRPrimer> getAllForwardPrimer();

  void addForwardPrimer(PCRPrimer val);
}
