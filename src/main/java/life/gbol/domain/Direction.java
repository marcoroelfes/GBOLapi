package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum Direction implements EnumClass {
  Forward("http://gbol.life/0.1/Forward",new Direction[]{}),

  Reverse("http://gbol.life/0.1/Reverse",new Direction[]{}),

  Both("http://gbol.life/0.1/Both",new Direction[]{});

  private Direction[] parents;

  private String iri;

  private Direction(String iri, Direction[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static Direction make(String iri) {
    for(Direction item : Direction.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
