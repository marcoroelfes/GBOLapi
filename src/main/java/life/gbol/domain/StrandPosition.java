package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum StrandPosition implements EnumClass {
  ForwardStrandPosition("http://gbol.life/0.1/ForwardStrandPosition",new StrandPosition[]{}),

  BothStrandsPosition("http://gbol.life/0.1/BothStrandsPosition",new StrandPosition[]{}),

  ReverseStrandPosition("http://gbol.life/0.1/ReverseStrandPosition",new StrandPosition[]{});

  private StrandPosition[] parents;

  private String iri;

  private StrandPosition(String iri, StrandPosition[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static StrandPosition make(String iri) {
    for(StrandPosition item : StrandPosition.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
