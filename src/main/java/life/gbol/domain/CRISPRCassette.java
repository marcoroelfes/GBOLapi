package life.gbol.domain;

import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface CRISPRCassette extends RepeatFeature {
  void remSpacer(Region val);

  List<? extends Region> getAllSpacer();

  void addSpacer(Region val);
}
