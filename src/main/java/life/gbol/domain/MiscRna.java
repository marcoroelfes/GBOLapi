package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface MiscRna extends Transcript {
  String getProduct();

  void setProduct(String val);
}
