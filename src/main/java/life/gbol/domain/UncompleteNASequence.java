package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface UncompleteNASequence extends NASequence {
  StrandType getStrandType();

  void setStrandType(StrandType val);
}
