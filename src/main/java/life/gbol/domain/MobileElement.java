package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface MobileElement extends RepeatFeature {
  String getMobileElementName();

  void setMobileElementName(String val);

  MobileElementType getMobileElementType();

  void setMobileElementType(MobileElementType val);
}
