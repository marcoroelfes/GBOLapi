package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface PCRPrimer extends OWLThing {
  String getPrimerName();

  void setPrimerName(String val);

  String getPrimerSequence();

  void setPrimerSequence(String val);
}
