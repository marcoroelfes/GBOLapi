package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface XRefProvenance extends Provenance {
  AnnotationLinkSet getOrigin();

  void setOrigin(AnnotationLinkSet val);
}
