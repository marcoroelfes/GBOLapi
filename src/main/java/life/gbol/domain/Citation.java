package life.gbol.domain;

import java.lang.String;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Citation extends Qualifier {
  Document getReference();

  void setReference(Document val);

  String getHighlight();

  void setHighlight(String val);
}
