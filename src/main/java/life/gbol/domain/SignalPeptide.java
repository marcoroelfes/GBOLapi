package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface SignalPeptide extends ProteinFeature {
  String getSignalTarget();

  void setSignalTarget(String val);
}
