package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum Topology implements EnumClass {
  Circular("http://gbol.life/0.1/Circular",new Topology[]{}),

  Linear("http://gbol.life/0.1/Linear",new Topology[]{});

  private Topology[] parents;

  private String iri;

  private Topology(String iri, Topology[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static Topology make(String iri) {
    for(Topology item : Topology.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
