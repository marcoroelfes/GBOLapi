package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum ProteinDomainType implements EnumClass {
  Family("http://gbol.life/0.1/Family",new ProteinDomainType[]{}),

  Domain("http://gbol.life/0.1/Domain",new ProteinDomainType[]{});

  private ProteinDomainType[] parents;

  private String iri;

  private ProteinDomainType(String iri, ProteinDomainType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static ProteinDomainType make(String iri) {
    for(ProteinDomainType item : ProteinDomainType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
