package life.gbol.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface FeatureProvenance extends Provenance {
  void remOnProperty(String val);

  List<? extends String> getAllOnProperty();

  void addOnProperty(String val);
}
