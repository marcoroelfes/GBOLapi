package life.gbol.domain;

import java.lang.Integer;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface MNP extends Variation {
  Integer getLength();

  void setLength(Integer val);
}
