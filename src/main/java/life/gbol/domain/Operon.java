package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Operon extends TranscriptionElement {
  String getOperonId();

  void setOperonId(String val);
}
