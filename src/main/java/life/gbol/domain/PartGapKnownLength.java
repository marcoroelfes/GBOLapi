package life.gbol.domain;

import java.lang.Integer;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface PartGapKnownLength extends PartGap {
  Integer getGapLength();

  void setGapLength(Integer val);
}
