package life.gbol.domain;

import java.util.List;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface GBOLLinkSet extends Entity {
  void remTarget(Database val);

  List<? extends Database> getAllTarget();

  void addTarget(Database val);
}
