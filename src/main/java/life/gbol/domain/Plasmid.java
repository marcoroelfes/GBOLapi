package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Plasmid extends CompleteNASequence {
  String getPlasmidName();

  void setPlasmidName(String val);
}
