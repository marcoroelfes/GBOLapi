package life.gbol.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Gene extends TranscriptionElement {
  void remGeneSymbolSynonym(String val);

  List<? extends String> getAllGeneSymbolSynonym();

  void addGeneSymbolSynonym(String val);

  void remExon(Exon val);

  List<? extends Exon> getAllExon();

  void addExon(Exon val);

  void remAllele(String val);

  List<? extends String> getAllAllele();

  void addAllele(String val);

  PseudoGeneType getPseudogene();

  void setPseudogene(PseudoGeneType val);

  String getOldLocusTag();

  void setOldLocusTag(String val);

  void remIntron(Intron val);

  List<? extends Intron> getAllIntron();

  void addIntron(Intron val);

  void remTranscript(Transcript val);

  List<? extends Transcript> getAllTranscript();

  void addTranscript(Transcript val);

  String getLocusTag();

  void setLocusTag(String val);

  String getGeneSymbol();

  void setGeneSymbol(String val);
}
