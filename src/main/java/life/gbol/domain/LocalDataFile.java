package life.gbol.domain;

import java.lang.String;
import java.time.LocalDateTime;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface LocalDataFile extends DataFile {
  String getLocalFileName();

  void setLocalFileName(String val);

  LocalDateTime getRetrievedOn();

  void setRetrievedOn(LocalDateTime val);

  String getLocalFilePath();

  void setLocalFilePath(String val);

  String getRetrievedFrom();

  void setRetrievedFrom(String val);
}
