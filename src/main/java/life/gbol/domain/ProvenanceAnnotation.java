package life.gbol.domain;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ProvenanceAnnotation extends OWLThing {
  void remReference(Document val);

  List<? extends Document> getAllReference();

  void addReference(Document val);

  void remDerivedFrom(Feature val);

  List<? extends Feature> getAllDerivedFrom();

  void addDerivedFrom(Feature val);

  String getProvenanceNote();

  void setProvenanceNote(String val);

  String getExperiment();

  void setExperiment(String val);
}
