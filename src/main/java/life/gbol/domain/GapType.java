package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum GapType implements EnumClass {
  HeterochromatinGap("http://gbol.life/0.1/HeterochromatinGap",new GapType[]{}),

  WithinScaffoldGap("http://gbol.life/0.1/WithinScaffoldGap",new GapType[]{}),

  UnknownGap("http://gbol.life/0.1/UnknownGap",new GapType[]{}),

  RepeatBetweeenScaffoldsGap("http://gbol.life/0.1/RepeatBetweeenScaffoldsGap",new GapType[]{}),

  BetweenScaffoldsGap("http://gbol.life/0.1/BetweenScaffoldsGap",new GapType[]{}),

  TelemereGap("http://gbol.life/0.1/TelemereGap",new GapType[]{}),

  RepeatWithinScaffoldGap("http://gbol.life/0.1/RepeatWithinScaffoldGap",new GapType[]{WithinScaffoldGap}),

  CentromereGap("http://gbol.life/0.1/CentromereGap",new GapType[]{}),

  ShortArmGap("http://gbol.life/0.1/ShortArmGap",new GapType[]{});

  private GapType[] parents;

  private String iri;

  private GapType(String iri, GapType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static GapType make(String iri) {
    for(GapType item : GapType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
