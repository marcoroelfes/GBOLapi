package life.gbol.domain;

import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface AnnotationResult extends Entity {
  AnnotationActivity getWasGeneratedBy();

  void setWasGeneratedBy(AnnotationActivity val);

  Agent getWasAttributedTo();

  void setWasAttributedTo(Agent val);
}
